#version 330 core
in vec2 fTexCoord;
in vec3 fColor;

out vec4 FragColor;

uniform sampler2D Texture;

void main()
{
	FragColor = texture2D(Texture, fTexCoord) * vec4(fColor, 1.0);
	if(FragColor.a < 0.1)
		discard;
}
