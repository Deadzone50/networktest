#version 330 core
in vec3 VertexColor;
in vec3 VertexNormal;
in vec3 FragPos;

out vec4 FragColor;

uniform vec3 CameraPos;


void main()
{
	float Dist = distance(CameraPos, FragPos);
	vec3 Color = clamp(3/(Dist*Dist), 0, 1) * VertexColor;
	FragColor = vec4(Color, 1.0);
}
