#version 330 core
layout (location = 0) in vec3 VPos;
layout (location = 1) in vec3 VCol;
layout (location = 2) in float VTime;

out vec3 VertexColor;
out float VertexTime;

uniform mat4 Projection;
uniform mat4 View;

void main()
{
	gl_Position = Projection * View * vec4(VPos, 1.0);

	VertexColor = VCol;
	VertexTime = VTime;
}
