#version 330 core
layout (location = 0) in vec3 VPos;
layout (location = 1) in vec3 VCol;
layout (location = 2) in vec2 VTexCoord;

uniform mat4 Projection;

out vec2 fTexCoord;
out vec3 fColor;

void main()
{
	gl_Position = Projection * vec4(VPos, 1.0);
	fTexCoord = VTexCoord;
	fColor = VCol;
}
