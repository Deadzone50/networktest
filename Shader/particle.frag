#version 330 core
in vec3 VertexColor;
in float VertexTime;

out vec4 FragColor;

void main()
{
	vec2 t = gl_PointCoord - vec2(0.5);
	float f =  dot(t, t);
	if (f > 0.2)
		discard;
	else
		FragColor = vec4(VertexColor, clamp(VertexTime, 0, 1));	
}
