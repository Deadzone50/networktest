#version 330 core
layout (location = 0) in vec3 VPos;
layout (location = 1) in vec3 VCol;
layout (location = 2) in vec3 VNorm;

out vec3 VertexColor;
out vec3 VertexNormal;
out vec3 FragPos;

uniform mat4 Model;
uniform mat4 View;
uniform mat4 Projection;

void main()
{
	gl_Position = Projection*View*Model * vec4(VPos, 1.0);
	VertexColor = VCol;
	VertexNormal = VNorm;	//TODO: rotate normal
	FragPos = vec3(Model * vec4(VPos, 1.0));
}
