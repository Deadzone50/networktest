#version 330 core
layout (location = 0) in vec3 VPos;
layout (location = 1) in vec2 TCoord;
//layout (location = 2) in vec3 VNorm;

out vec2 TexCoord;
//out vec3 VertexNormal;
out vec3 FragPos;

uniform mat4 View;
uniform mat4 Projection;

uniform vec3 Pos;

uniform float Scale;
uniform float TextureScale;
uniform int AnimFrame;
uniform int RotationIndex;

void main()
{
	mat4 Model = mat4(Scale);	//set identity scale
	Model[3] = vec4(Pos,1);		//set translation

	mat4 ModelView = View*Model;
	float xScale = length(ModelView[0].xyz);	//preserve scaling for x and z
	float zScale = length(ModelView[2].xyz);
	ModelView[0][0] = xScale;
	ModelView[0][1] = 0;
	ModelView[0][2] = 0;
//
//	ModelView[1][0] = 0;
//	ModelView[1][1] = yScale;
//	ModelView[1][2] = 0;

	ModelView[2][0] = 0;
	ModelView[2][1] = 0;
	ModelView[2][2] = zScale;

	gl_Position = Projection*ModelView * vec4(VPos, 1.0);

//	TexCoord = vec2(0.1*AnimFrame + TCoord.x/10, 0.1*RotationIndex + TCoord.y/10);
	TexCoord = vec2(TextureScale*(AnimFrame + TCoord.x), TextureScale*(RotationIndex + TCoord.y));
//	VertexNormal = VNorm;	//TODO: rotate normal
	FragPos = (Model * vec4(VPos, 1.0)).xyz;
}
