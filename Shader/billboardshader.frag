#version 330 core
in vec2 TexCoord;
//in vec3 VertexNormal;
in vec3 FragPos;

out vec4 FragColor;

uniform vec3 CameraPos;

uniform sampler2D Texture;

void main()
{
	float Dist = distance(CameraPos, FragPos);

	vec4 Tex = texture(Texture, TexCoord);
	vec4 Color = vec4(clamp(3/(Dist*Dist), 0, 1) * Tex.xyz, Tex.a);
	if(Color.a < 0.1)
	{
		discard;
	}
	FragColor = Color;
}
