#version 330 core
layout (location = 0) in vec3 VPos;
layout (location = 1) in vec2 VTexCoord;
layout (location = 2) in vec3 VCol;

uniform mat4 View;
uniform mat4 Projection;

out vec2 fTexCoord;
out vec3 fColor;

void main()
{
	gl_Position = Projection*View * vec4(VPos, 1.0);
	fTexCoord = VTexCoord;
	fColor = VCol;
}
