#version 330 core
layout (location = 0) in vec3 VPos;
layout (location = 1) in vec2 TCoord;

out vec2 TexCoord;

uniform mat4 Projection;

uniform int AnimFrame;
uniform int Row;
//uniform vec3 Pos;

void main()
{
//	mat4 Model = mat4(1.0);
//	Model[3] = vec4(Pos,1);
	gl_Position = Projection * vec4(VPos, 1.0);

	TexCoord = vec2(0.25*AnimFrame + TCoord.x/4, 0.25*Row + TCoord.y/4);
}
