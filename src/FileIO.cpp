#include "FileIO.h"

#include <map>
#include <iostream>
#include <fstream>
void
ParseObjFile(const std::string &Path, const std::string &Name, std::vector<Mesh> &Meshes)
{
	std::vector<Vec3f> VertexPositions;
	std::vector<Vec2f> TextureCordinates;
	std::vector<Vec3f> VertexNormals;

	std::string Line;
	std::string Word;
	Vec3f V3;
	Vec2f V2;

	std::string ObjectName = "";
	std::string MaterialName;
	std::map<std::string, unsigned int> MaterialMap;

	std::vector<Vertex> *Vertices;
	std::vector<uint32_t> *Indices;
	Mesh *CurrentMesh;

	Meshes.push_back(Mesh{});				//setup first mesh
	CurrentMesh = &Meshes.back();
	Vertices = &Meshes.back().Vertices;
	Indices = &Meshes.back().Indices;

	std::ifstream file;
	file.open(Path+Name);
	if(!file.is_open())
	{
		std::cout << Path+Name << " not found\n";
	}

	while(std::getline(file, Line))
	{
		if((Line[0] == '#') || (Line[0] == '\0'))	//skip comments and empty lines
			continue;
		std::istringstream iss(Line);
		iss >> Word;
		if(Word == "v")								//vertex, assumes no w cordinate
		{
			iss >> V3;
			VertexPositions.push_back(V3);
		}
		else if(Word == "vt")						//texture coordinate, assumes no w cordinate
		{
			iss >> V2;
			TextureCordinates.push_back(V2);
		}
		else if(Word == "vn")						//vertex normal
		{
			iss >> V3;
			VertexNormals.push_back(V3);
		}
		else if (Word == "f")						//face element
		{
			int FV[4] = {0};
			int FT[4] = {0};
			int FN[4] = {0};
			for(int i = 0; i<4; ++i)
			{
				iss >> FV[i];						//extract the vertex index
				if (iss.peek() == '/')				//if there are texture, and normal indices
				{
					iss.ignore();
					if(iss.peek() == '/')			//normal index
					{
						iss.ignore();
						iss >> FN[i];
					}
					else
					{								//texture index
						iss >> FT[i];
						if(iss.peek() == '/')		//normal index
						{
							iss.ignore();
							iss >> FN[i];
						}
					}
				}
			}

			if(!FV[3])								//only 3 vertices
			{
				Vertex Vert[3]{};
				Vert[0].Position = VertexPositions[FV[0]-1];
				Vert[1].Position = VertexPositions[FV[1]-1];
				Vert[2].Position = VertexPositions[FV[2]-1];
				if(FN[0])
				{
					Vert[0].Normal = VertexNormals[FN[0]-1];
					Vert[1].Normal = VertexNormals[FN[1]-1];
					Vert[2].Normal = VertexNormals[FN[2]-1];
				}
				if(FT[0])
				{
					Vert[0].TexCoord = TextureCordinates[FT[0]-1];
					Vert[1].TexCoord = TextureCordinates[FT[1]-1];
					Vert[2].TexCoord = TextureCordinates[FT[2]-1];
				}

				int Found[3] = {-1, -1, -1};
				unsigned int VertI[3];

				int SIZE = (int)Vertices->size();
				if(SIZE)
				{
					for(int i2 = SIZE-1; i2 >= 0; --i2)
					{
						for(int i1 = 0; i1 < 3; ++i1)
						{
							if((*Vertices)[i2] == Vert[i1])
							{
								Found[i1] = i2;
								VertI[i1] = i2;
								break;
							}
						}
						if(Found[0] > -1 && Found[1] > -1 && Found[2] > -1)
							break;
					}
				}

				if(Found[0] == -1)
				{
					VertI[0] = (unsigned int)Vertices->size();
					Vertices->push_back(Vert[0]);
				}
				if(Found[1] == -1)
				{
					VertI[1] = (unsigned int)Vertices->size();
					Vertices->push_back(Vert[1]);
				}
				if(Found[2] == -1)
				{
					VertI[2] = (unsigned int)Vertices->size();
					Vertices->push_back(Vert[2]);
				}

				Indices->push_back(VertI[0]);
				Indices->push_back(VertI[1]);
				Indices->push_back(VertI[2]);
			}
			else
				abort();
		}
		else if(Word == "o")						//object
		{
			if(ObjectName != "")					//if not hte first
			{
				Meshes.push_back(Mesh{});
				CurrentMesh = &Meshes.back();
				Vertices = &Meshes.back().Vertices;
				Indices = &Meshes.back().Indices;
			}
			iss >> ObjectName;
		}
		else if(Word == "usemtl")					//material
		{
			iss >> MaterialName;
			//CurrentMesh->MaterialIndex = MaterialMap[MaterialName];
		}
		else if(Word == "mtllib")					//material file
		{
			iss >> Word;
			//ParseMtlFile(FullPath, Word, Materials, MaterialMap);
		}
		else										//unknown obj property
		{
			continue;
		}
	}
	file.close();
}