#pragma once

#include <GL/glew.h>
#include <GL/glut.h>

#include <vector>
#include "Vector/Vector.h"
#include "Shader.h"

class DebugDraws
{
public:
	void Initialize();

	void DrawPoint(Vec3f P, Vec3f Color);
	void DrawLine(Vec3f S, Vec3f E, Vec3f Color);
	void DrawTriangle(Vec3f V1, Vec3f V2, Vec3f V3, Vec3f Color);

	void UpdateBuffers();
	void Clear();
	void Enable() { m_Enabled = !m_Enabled; }

	void DrawDebugGraphics(Shader &Shader);
private:
	std::vector<uint32_t> m_IndicesPoints;
	std::vector<uint32_t> m_IndicesLines;
	std::vector<Vec3f> m_VerticesPoints;
	std::vector<Vec3f> m_VerticesLines;

	uint32_t m_VAOP, m_VBOP, m_EBOP;
	uint32_t m_VAOL, m_VBOL, m_EBOL;

	bool m_Enabled = false;
	bool m_Update = false;
};
