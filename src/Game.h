#pragma once

#include "Menu.h"
#include "Map.h"
#include "ParticleSystem.h"
#include "Network.h"
#include "Player.h"

enum class GameState { DisplayGame, DisplayMenu, DisplaySettings, DisplayMultiplayer };

enum class GameType { Host, Client };

class Game
{
public:
	void InitGame();
	void Reset();
	GameState GetState() { return m_CurrentState; }
	Menu *GetCurrentMenu() { return m_CurrentMenu; }
	Map &GetCurrentLevel() { return m_Level; }

	void InitMenus();

	void SwitchState(GameState State);

	void Exit();

	void HighLight(Vec3f Origin, Vec3f Dir, float MaxRange);
	void Interact(Vec3f Origin, Vec3f Dir, float Max);
	void Attack(Vec3f Origin, Vec3f Dir, float Damage, AttackType Type, float MaxRange);

	bool IsRunning() { return m_Started; }
	void Stop() { m_Started = false; }

	void Step(float dt);
	void Draw(const Mat4f &View, const Mat4f &Projection);
	void DrawItems(Shader &Shader);

	void AddPlayer(PlayerClass *Player) { m_Players.push_back(Player); }
	std::vector<PlayerClass*> &GetPlayers() { return m_Players; }

	void AddParticle(Vec3f Pos, Vec3f Col, Vec3f Speed, float Time);

	GameType GetGameType() { return m_Type; }

	void AddEvent(GameEvent Event);
	std::vector<GameEvent> &GetEvents() {return m_Events;}
	void ClearEvents() {m_Events.clear();}
	void ProcessEvent(GameEvent Event);

	void SetState(uint32_t Id, CharacterState State);
	void ChangePlayerHealth(uint32_t Id, float Amount);
	void ChangePlayerAmmo(uint32_t Id, int32_t Amount);
	void RemoveItem(uint32_t Id);

	void DrawUI(Shader &Shader);
private:
	Map m_Level;
	GameState m_CurrentState;

	std::vector<Menu*> m_Menus;
	Menu *m_CurrentMenu;

	std::string m_Other_IP = "127.0.0.1";
	std::string m_Port = "30000";

	bool m_Started = false;

	ParticleSystem m_ParticleSystem;
	std::vector<PlayerClass*> m_Players;

	GameType m_Type = GameType::Host;

	std::vector<GameEvent> m_Events;

	uint32_t m_VAO, m_VBO, m_EBO;
	std::vector<Vec3f> m_Vertices;	//Pos, Col
	std::vector<uint32_t> m_Indices;
};
