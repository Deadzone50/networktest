#include "Menu.h"

MenuItem::MenuItem(Vec2i Corners[2], MenuItemType Type)
{
	m_Corners[0] = Corners[0];
	m_Corners[1] = Corners[1];
	m_Type = Type;
}

void MenuItem::ConnectValue(std::string *Value)
{
	m_Value = Value;
}

void MenuItem::ConnectOnClick(std::function<void()> OnClick)
{
	m_OnClick = OnClick;
}

void MenuItem::SetValue(std::string Value)
{
	*m_Value = Value;
}

std::string MenuItem::GetValue()
{
	return *m_Value;
}


bool MenuItem::Click(Vec2i Pos)
{
	if(m_Corners[0].x <=Pos.x && m_Corners[1].x >= Pos.x &&
			m_Corners[0].y <=Pos.y && m_Corners[1].y >= Pos.y)
	{
		if(m_OnClick != nullptr)
			m_OnClick();
		return true;
	}
	else
		return false;
}


Menu::Menu()
{
	m_Shader = Shader("Shader/menu.vert", "Shader/menu.frag");
	m_ProjectionMatrix = Ortho(0, 800, 0, 600, -1, 1);
	m_Shader.Use();
	m_Shader.SetMat4("Projection", m_ProjectionMatrix);
	m_Shader.SetSampler("Texture", 1);

	glGenVertexArrays(1, &m_VAO);
	glGenBuffers(1, &m_VBO);
	glGenBuffers(1, &m_EBO);
}

static inline void AddQuad(Vec2i Corners[2], float Z, Vec3f Color, std::vector<Vertex> &Vertices, std::vector<uint32_t> &Indices)
{
	uint32_t index = Vertices.size();

	Vertices.push_back({{(float)Corners[0].x, (float)Corners[0].y, Z}, Color});
	Vertices.push_back({{(float)Corners[1].x, (float)Corners[0].y, Z}, Color});
	Vertices.push_back({{(float)Corners[1].x, (float)Corners[1].y, Z}, Color});
	Vertices.push_back({{(float)Corners[0].x, (float)Corners[1].y, Z}, Color});

	Indices.push_back(index);
	Indices.push_back(index+1);
	Indices.push_back(index+2);
	Indices.push_back(index+3);
}

void Menu::Draw()
{
	glBindVertexArray(m_VAO);
	m_Shader.Use();		//draw scene

	if(m_Changed)
	{
		m_Vertices.clear();
		m_Indices.clear();
		for(auto &I : m_MenuItems)
		{
			AddQuad(I.m_Corners, 0, {0.5f, 0.5f, 0.5f},m_Vertices, m_Indices);

			Vec3f TextPos = Vec3f{(float)I.m_Corners[0].x +30, (float)I.m_Corners[0].y +10, 0.2f};
			CreateTextMesh(I.m_Text, TextPos, {1,0,0}, {0,1,0}, 2, {1,0,0}, m_Vertices, m_Indices);

			if(I.m_Type == MenuItemType::Input)
			{
				Vec3f Pos = (m_Vertices.end()-3)->Position;
				std::string tmp = I.GetValue();
				CreateTextMesh(tmp, Pos+Vec3f{10}, {0,1,0}, {1,0,0}, 2, {0,1,0}, m_Vertices, m_Indices);
			}

		}
		if(m_Selected >= 0)
		{
			Vec2i Corners[2] = {m_MenuItems[m_Selected].m_Corners[0], m_MenuItems[m_Selected].m_Corners[0]+Vec2i{10,10}};
			AddQuad(Corners, 0.5f, {1, 1, 1}, m_Vertices, m_Indices);

		}
		glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex)*m_Vertices.size(), m_Vertices.data(), GL_STATIC_DRAW);

		// position
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Position));
		glEnableVertexAttribArray(0);
		// Color
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Color));
		glEnableVertexAttribArray(1);
		// Texcoord
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, TexCoord));
		glEnableVertexAttribArray(2);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_EBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint32_t)*m_Indices.size(), m_Indices.data(), GL_STATIC_DRAW);
		m_Changed = false;
	}
	glDrawElements(GL_QUADS, m_Indices.size(), GL_UNSIGNED_INT, (void*)0);
}

void Menu::AddButton(const std::string &Text, std::function<void()> OnClick)
{
	Vec2i Corners[2] = {};

	Corners[1].y = m_ScreenSize.y -(m_Padding + m_MenuItems.size()*(m_Padding + m_Height));
	Corners[0].y = Corners[1].y - m_Height;

	Corners[0].x = m_Padding;
	Corners[1].x = m_Width - m_Padding;

	MenuItem B(Corners, MenuItemType::Button);
	B.ConnectOnClick(OnClick);
	B.m_Text = Text;
	m_MenuItems.push_back(B);

	m_Changed = true;
}

void Menu::ChangeButton(uint32_t index, const std::string &Text, std::function<void()> OnClick)
{
	m_MenuItems[index].m_Text = Text;
	m_MenuItems[index].ConnectOnClick(OnClick);
	m_Changed = true;
}

void Menu::AddInput(const std::string &Text, std::string *Value)
{
	Vec2i Corners[2] = {};

	Corners[1].y = m_ScreenSize.y -(m_Padding + m_MenuItems.size()*(m_Padding + m_Height));
	Corners[0].y = Corners[1].y - m_Height;

	Corners[0].x = m_Padding;
	Corners[1].x = m_Width - m_Padding;

	MenuItem B(Corners, MenuItemType::Input);
	B.ConnectValue(Value);
	B.m_Text = Text;
	m_MenuItems.push_back(B);

	m_Changed = true;
}

void Menu::Click(Vec2i Pos)
{
	for(int i = 0; i < m_MenuItems.size(); ++i)
	{
		if(m_MenuItems[i].Click({Pos.x, Pos.y}))
		{
			m_Selected = i;
			m_Changed = true;
			break;
		}
	}
}

void Menu::Update(int Width, int Height)
{
	m_ScreenSize = {Width, Height};
}

void Menu::ChangeInput(char c)
{
	if(m_Selected >= 0)
	{
		std::string v = m_MenuItems[m_Selected].GetValue();
		if(c == 8)			//Backspace
		{
			if(!v.empty())
				v.pop_back();
		}
		else
			v += c;

		m_MenuItems[m_Selected].SetValue(v);
		m_Changed = true;
	}
}

void Menu::SetInput(std::string s)
{
	if(m_Selected >= 0)
	{
		std::string v = m_MenuItems[m_Selected].GetValue();
		m_MenuItems[m_Selected].SetValue(v+s);
		m_Changed = true;
	}
}
