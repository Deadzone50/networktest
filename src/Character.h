#pragma once

#include "Vector/Vector.h"
#include "Shader.h"

enum class CharacterState : uint32_t
{
	None = 0x00,
	Animating = 0x01,
	Walking = 0x02,
	Attacking = 0x04,
	Damaged = 0x08,
	Idle = 0x10,
	Dead = 0x20,
};

bool operator !(const CharacterState &rhs);
CharacterState operator &(CharacterState lhs, CharacterState rhs);
CharacterState operator |(CharacterState lhs, CharacterState rhs);
CharacterState operator ~(CharacterState rhs);

class Character
{
public:
	Character();

	void SetPosition(const Vec3f &Pos) {m_Pos = Pos;}
	Vec3f GetPosition() const {return m_Pos;}

	void SetFacingDirection(Vec2f FacingDirection);
	Vec2f GetFacingDirection() const { return m_LookingDirection2D; }

	void Draw(Shader &Shader, Vec3f CameraPos) const;

	bool RayCollision(Vec3f Origin, Vec3f Direction, float &Max);

	Vec3f HandleCollision(float dt);

	uint32_t GetId() {return m_Id;}

	bool IsAlive() { return !(m_State & CharacterState::Dead); }

protected:
	Vec2f m_LookingDirection2D, m_Right2D;

	Vec3f m_Pos, m_PrevPos, m_CurrentSpeed;

	bool m_Falling = true;

	uint32_t m_Id, m_TextureId;
	uint8_t m_CurrentFrame = 0;

	CharacterState m_State = CharacterState::Walking;

	uint32_t m_Ammo = 20;
	float m_Health = 100;
};
