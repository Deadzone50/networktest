#include "Enemy.h"

#include "Player.h"
#include "TextureManager.h"
#include "Map.h"
#include "Random.h"
#include "DebugDraws.h"
#include "Game.h"
#include "Sound.h"

extern TextureManager TexManager;
extern DebugDraws DebugDrawer;
extern RandomGenerator RandGen;
extern Game App;
extern SoundManager AudioManager;

EnemyClass::EnemyClass(EnemyType Type, Vec3f Pos)
{
	//m_LookingDirection = {0,0,-1};
	m_LookingDirection2D = {0,-1};
	m_Right2D = {1,0};
	m_CurrentSpeed = {0,0,0};

	m_Pos = Pos;
	m_PrevPos = Pos;
	m_Type = Type;
	switch(Type)
	{
		case Cultist:
			m_TextureId = TexManager.Texture("Assets/Sprites/Cult1.png");
			break;
		case Mage:
			m_TextureId = TexManager.Texture("Assets/Sprites/Mage.png");
			break;
	}
	m_DeathTime = RandGen.GetFloat(0.2f, 0.4f);
}

void EnemyClass::Step(float dt)
{
#if 1
	if(m_State == CharacterState::Walking)
	{
		//Cycle anim
		m_AnimTime += dt;
		if(m_AnimTime > 0.15f)
		{
			m_CurrentFrame = (m_CurrentFrame+1)%4;
			m_AnimTime = 0;
		}
		if(App.GetGameType() == GameType::Host)
		{
			if(m_WanderTime > 0)
				m_WanderTime -= dt;
			if(m_WanderTime <= 0)
			{
				std::vector<PlayerClass*> Players = App.GetPlayers();		//Find closest player
				PlayerClass *ClosestPlayer = nullptr;
				float ClosestDist = FLT_MAX;
				for(auto PP : Players)
				{
					if(PP->IsAlive())
					{
						float dist = LEN(m_Pos - PP->GetPosition());
						if(dist < ClosestDist)
						{
							ClosestDist = dist;
							ClosestPlayer = PP;
						}
					}
				}
				if(ClosestPlayer == nullptr)
				{
					if(LEN(m_Goal - m_Pos) < m_Speed)
					{
						m_Goal = m_Pos + RandGen.GetV3f(-10, 10);
						m_Goal.y = 0;
					}
				}
				else
				{
					m_Goal = ClosestPlayer->GetPosition();
					if(ClosestDist < 1.5f)
					{
						GameEvent E = CreateCharacterStateEvent(m_Id, CharacterState::Attacking);
						App.AddEvent(E);
					}
				}
			}
			Vec3f Dir = NORMALIZE(m_Goal - m_Pos);
			m_LookingDirection2D = NORMALIZE(Vec2f{Dir.x, Dir.z});
			m_Right2D = Rotate90CCW(m_LookingDirection2D);
			m_Pos += dt*m_Speed*Vec3f{m_LookingDirection2D.x, 0, m_LookingDirection2D.y};
			Vec3f NewPos = HandleCollision(dt);
			if(LEN2(NewPos*Vec3f{1,0,1}))
			{
				Vec2f r = RandGen.GetV2f(-10, 10);
				Vec2f tmp2 = r.x*m_Right2D + r.y*m_LookingDirection2D;
				m_Goal = m_Pos + Vec3f{tmp2.x, 0, tmp2.y};
				m_WanderTime = RandGen.GetFloat(0.5f, 1);
			}
			m_PrevPos = m_Pos + NewPos;
			m_Pos = m_Pos + NewPos + m_CurrentSpeed*dt;
		}
	}
	else if(m_State == CharacterState::Attacking)
	{
		m_AnimTime += dt;
		if(m_AnimTime > 0.20f*(m_CurrentFrame-3))
		{
			m_CurrentFrame++;
			if(App.GetGameType() == GameType::Host)
			{
				if(m_CurrentFrame-4 > 3)
				{
					Attack();
					GameEvent E = CreateCharacterStateEvent(m_Id, CharacterState::Walking);
					App.AddEvent(E);
				}
			}
		}
	}
	else if(m_State == CharacterState::Damaged)
	{
		if(App.GetGameType() == GameType::Host)
		{
			m_AnimTime += dt;
			if(m_AnimTime > 0.4f)
			{
				SetState(CharacterState::Walking);
			}
		}
	}
	else if(m_State == CharacterState::Dead)
	{
		m_AnimTime += dt;
		if(m_AnimTime < 6*m_DeathTime)
		{
			if(m_AnimTime > (m_CurrentFrame+1)*m_DeathTime)
			{
				m_CurrentFrame++;
			}
		}
	}
	if(App.GetGameType() == GameType::Host && m_State != CharacterState::Damaged && m_State != CharacterState::Dead &&
	   m_DamageTaken > 0)
	{
		m_Health -= m_DamageTaken;
		m_WanderTime = 0;
		float r = RandGen.GetFloat(0,1);
		if(r*m_DamageTaken > 10.0f)
		{
			GameEvent E = CreateCharacterStateEvent(m_Id, CharacterState::Damaged);
			App.AddEvent(E);
			AudioManager.Play("Pain", m_Pos);
		}

		if(m_Health <= 0)
		{
			GameEvent E = CreateCharacterStateEvent(m_Id, CharacterState::Dead);
			App.AddEvent(E);
			AudioManager.Play("Death", m_Pos, 0.5f);
		}
		m_DamageTaken = 0;
	}
#endif
}

void EnemyClass::SetState(CharacterState State)
{
	m_State = State;
	m_AnimTime = 0;
	if(State == CharacterState::Walking)
	{
		m_CurrentFrame = 0;
		m_Speed = 3.0f;
	}
	else if(State == CharacterState::Attacking)
	{
		m_CurrentFrame = 4;
		m_Speed = 0;
	}
	else if(State == CharacterState::Damaged)
	{
		m_CurrentFrame = 8;
	}
	else if(State == CharacterState::Dead)
	{
		m_CurrentFrame = 0;
		m_Health = 0;
		m_AnimTime = 0;
	}
}

void EnemyClass::Damage(float Damage)
{
	if(App.GetGameType() == GameType::Host)
	{
		m_DamageTaken += Damage;
	}
}

void EnemyClass::Attack()
{
	Vec3f Dir = {m_LookingDirection2D.x, 0, m_LookingDirection2D.y};
	GameEvent E = CreateAttackEvent(WeaponType::Pitchfork, m_Pos-Vec3f{0,0.4f,0}, Dir);
	App.AddEvent(E);

	float r = RandGen.GetFloat(0, 1);
	if(r < 0.5f)
		AudioManager.Play("Stab-01", m_Pos);
	else
		AudioManager.Play("Stab-02", m_Pos);
}
