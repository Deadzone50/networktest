#pragma once
#include <GL/glew.h>
#include <GL/glut.h>
#include "Vector/Vector.h"
#include "Player.h"

extern bool KeyStates[256];
extern bool KeySpecialStates[256];
extern bool KeyChanged[256];

extern bool InitMouse;
extern Vec2i MousePos, MouseDelta;
extern Vec3f MousePos3d, MouseDir;
extern bool MouseButton[5];
extern bool MouseChanged[5];

extern Vec2i ScreenSize;
extern PlayerClass *Player;

#define GLUT_KEY_F1					0x0001
#define GLUT_KEY_F2					0x0002
#define GLUT_KEY_F3					0x0003
#define GLUT_KEY_F4					0x0004
#define GLUT_KEY_F5					0x0005
#define GLUT_KEY_F6					0x0006
#define GLUT_KEY_F7					0x0007
#define GLUT_KEY_F8					0x0008
#define GLUT_KEY_F9					0x0009
#define GLUT_KEY_F10				0x000A
#define GLUT_KEY_F11				0x000B
#define GLUT_KEY_F12				0x000C
#define GLUT_KEY_LEFT				0x0064
#define GLUT_KEY_UP					0x0065
#define GLUT_KEY_RIGHT				0x0066
#define GLUT_KEY_DOWN				0x0067
#define GLUT_KEY_PAGE_UP			0x0068
#define GLUT_KEY_PAGE_DOWN			0x0069
#define GLUT_KEY_HOME				0x006A
#define GLUT_KEY_END				0x006B
#define GLUT_KEY_INSERT				0x006C
#define GLUT_KEY_SHIFT				112
#define GLUT_KEY_CTRL				114

void KeyDown(unsigned char Key, int MouseX, int MouseY);

void KeyUp(unsigned char Key, int MouseX, int MouseY);

void KeySpecialDown(int Key, int MouseX, int MouseY);

void KeySpecialUp(int Key, int MouseX, int MouseY);

void MouseFunc(int button, int state, int x, int y);

void MousePassiveMove(int x, int y);

void HandleMouse(float dt);

void HandleKeys(float dt);

void ResetMouse();
void HideMouse();
void ShowMouse();
