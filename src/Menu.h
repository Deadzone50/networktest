#pragma once

#include <GL/glew.h>
#include <GL/glut.h>

#include <string>
#include <vector>
#include <functional>

#include "Vector/Matrix.h"
#include "Vector/Vector.h"
#include "Shader.h"
#include "Vertex.h"

void CreateTextMesh(std::string Text, Vec3f Position, Vec3f Color, std::vector<Vertex> &Vertices, std::vector<uint32_t> &Indices);

enum class MenuItemType { Input, Button };

class MenuItem
{
public:
	MenuItem(Vec2i Corners[2], MenuItemType Type);
	void ConnectOnClick(std::function<void()> OnClick);
	void ConnectValue(std::string *Value);
	void SetValue(std::string Value);
	std::string GetValue();
	bool Click(Vec2i Pos);

	Vec2i m_Corners[2];
	std::function<void()> m_OnClick = nullptr;
	std::string *m_Value;
	MenuItemType m_Type;
	std::string m_Text;
};


class Menu
{
public:
	Menu();
	void Draw();
	void AddButton(const std::string &Text, std::function<void()> OnClick);
	void ChangeButton(uint32_t index, const std::string &Text, std::function<void()> OnClick);
	void AddInput(const std::string &Text, std::string *Value);
	void Click(Vec2i Pos);
	void Update(int Width, int Height);

	void ChangeInput(char c);
	void SetInput(std::string s);

	void Reset() {m_Changed = true; m_Selected = -1; }
private:
	std::vector<MenuItem> m_MenuItems;
	int32_t m_Selected = -1;
	std::vector<Vertex> m_Vertices;
	std::vector<uint32_t> m_Indices;
	Shader m_Shader;
	unsigned int m_VAO, m_VBO, m_EBO;

	uint32_t m_Padding = 10, m_Height = 50, m_Width = 400;
	Vec2i m_ScreenSize = {800, 600};
	Mat4f m_ProjectionMatrix;

	bool m_Changed = true;
};

