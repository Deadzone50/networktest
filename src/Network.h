#pragma once

#include <winsock2.h>
#include <vector>
#include "Character.h"
#include "Weapon.h"

bool InitializeSockets();

void TerminateSockets();

enum class EventType : uint32_t
{
	Attack, CharacterState, PlayerHealth, PlayerAmmo, ItemPickup
};

//-AttackEvent
//WeaponType
//Pos
//Dir

//-StateEvent
//Id
//State

//-HealthEvent
//Id
//Amount

//-AmmoEvent
//Id
//Amount

//-ItemEvent
//Item Id

struct GameEvent
{
	EventType Type;
	std::vector<uint32_t> Data;
};

class Address
{
public:
	Address() {m_address = 0; m_port = 0;}
	Address(unsigned char a, unsigned char b, unsigned char c, unsigned char d, unsigned short port);
	Address(unsigned int address, unsigned short port);

	unsigned char GetA() const;
	unsigned char GetB() const;
	unsigned char GetC() const;
	unsigned char GetD() const;

	unsigned int GetAddress() const;
	unsigned short GetPort() const;
private:
	unsigned int m_address;
	unsigned short m_port;
};

class Socket
{
public:
	Socket();
	~Socket();

	bool Open(unsigned short port);
	void Close();
	bool Send(const Address &destination, const void *data, int size);
	int Receive(Address &source, void *data, int size);
private:
	SOCKET m_handle;

};

bool EstablishConnection(Socket *Sock, Address Addr);

void CharacterDataToBuffer(uint8_t *Buffer, uint32_t &offset, const Character *Char);

void CharacterDataFromBuffer(uint8_t *Buffer, uint32_t &offset, Character *Char);

void EventToBuffer(uint8_t *Buffer, uint32_t &offset, const GameEvent &Event);

void EventFromBuffer(uint8_t *Buffer, uint32_t &offset, GameEvent &Event);

GameEvent CreateAttackEvent(WeaponType Type, Vec3f Pos, Vec3f Dir);

GameEvent CreateHealthEvent(uint32_t Id, float Health);
GameEvent CreateAmmoEvent(uint32_t Id, int32_t Ammo);

GameEvent CreateCharacterStateEvent(uint32_t Id, CharacterState State);

GameEvent CreateItemEvent(uint32_t ItemId);

void ClientSync(Socket *Sock);

void HostSync(Socket *Sock);
