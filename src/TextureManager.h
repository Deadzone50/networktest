#pragma once

#include <GL/glew.h>
#include <GL/glut.h>

#include <string>
#include <map>

class TextureManager
{
public:
	TextureManager();
	uint32_t Texture(const std::string &FileName);

private:
	std::map<std::string, uint32_t> m_Textures;
};

