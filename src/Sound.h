#pragma once

#include <string>
#include <vector>
#include <map>

#include "Vector/Vector.h"

class SoundManager
{
public:
	void Initialize();
	void LoadSoundFile(const std::string &Name);
	void Play(const std::string &Name, Vec3f Position, float Volume = 1.0f, float Pitch = 1.0f);
	void SetVolume(float Vol) { m_Volume = Vol; }
	float GetVolume() { return m_Volume; }

private:
	std::map<std::string, uint32_t> m_SoundMap;
	std::vector<uint32_t> m_Sources;
	float m_Volume = 0.4f;
};

bool
LoadWavFile(const std::string &Name, uint32_t &Channels, uint32_t &SampleRate, uint32_t &BitsPerSample, std::vector<char> &SoundData);
