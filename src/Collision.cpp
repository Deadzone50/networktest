#include "Collision.h"

int RayTriangleIntersection(Vec3f Origin, Vec3f Direction, Vec3f Triangle[3], float &tmax)
{
	Vec3f N = NORMALIZE(CROSS(Triangle[1]-Triangle[0], Triangle[2]-Triangle[0]));
	float Denominator = DOT(N, Direction);
	if(fabsf(Denominator) < 0.0001f)	//parallel
		return 0;

	float D = DOT(N, Triangle[0]);
	float t = -(DOT(N, Origin) - D) / Denominator;

	if(t < 0 || t >= tmax)	//behind or too far away
		return 0;

	Vec3f Cross;

	Vec3f P = Origin+t*Direction;
	Vec3f Edge0 = Triangle[1] - Triangle[0];	//check if point is inside the triangle
	Vec3f ToPoint0 = P - Triangle[0];
	Cross = CROSS(Edge0, ToPoint0);
	if(DOT(N, Cross) < 0)
		return 0;

	Vec3f Edge1 = Triangle[2] - Triangle[1];	//check if point is inside the triangle
	Vec3f ToPoint1 = P - Triangle[1];
	Cross = CROSS(Edge1, ToPoint1);
	if(DOT(N, Cross) < 0)
		return 0;

	Vec3f Edge2 = Triangle[0] - Triangle[2];	//check if point is inside the triangle
	Vec3f ToPoint2 = P - Triangle[2];
	Cross = CROSS(Edge2, ToPoint2);
	if(DOT(N, Cross) < 0)
		return 0;

	tmax = t;
	if(Denominator > 0)
		return -1;	//backside of triangle hit
	else
		return 1;
}

bool
RayAABBCollisionTest(Vec3f RayOrigin, Vec3f RayDir, Vec3f AABB[2], float& MinLen, float& MaxLen)
{
	// RO.x + t*RD.x == Min.x -> collision with zy-plane
	// t = Min.x - RO.x / RD.x

	Vec3f InvDir;					//precompute inverse of direction			
	InvDir.x = 1.0f/RayDir.x;		//floating point division by zero defined: +Inf
	InvDir.y = 1.0f/RayDir.y;
	InvDir.z = 1.0f/RayDir.z;

	Vec3i Swap;						//precalculate if the bounds have to be swapped
	Swap.x = (RayDir.x < 0);
	Swap.y = (RayDir.y < 0);
	Swap.z = (RayDir.z < 0);


	float T1X = (AABB[Swap.x].x - RayOrigin.x)*InvDir.x;		//intersection x	//Ro + Rd*t ,t[0,Tmax]
	float T2X = (AABB[1-Swap.x].x - RayOrigin.x)*InvDir.x;

	float T1Y = (AABB[Swap.y].y - RayOrigin.y)*InvDir.y;		//intersection y
	float T2Y = (AABB[1-Swap.y].y - RayOrigin.y)*InvDir.y;

	if(T1X < T1Y)				//max of mins
		T1X = T1Y;
	if(T2X > T2Y)				//min of maxs
		T2X = T2Y;

	float T1Z = (AABB[Swap.z].z - RayOrigin.z)*InvDir.z;		//intersection z
	float T2Z = (AABB[1-Swap.z].z - RayOrigin.z)*InvDir.z;

	if(T1X < T1Z)				//max of mins
		T1X = T1Z;
	if(T2X > T2Z)				//min of maxs
		T2X = T2Z;

	if(T1X > MaxLen)				//check that it isn't too far
		return false;
	if(T1X > T2X)				//miss
	{
		return false;
	}
	else if(T2X < 0)			//behind, miss
	{
		return false;
	}
	else if(T1X > 0)			//hit
	{
		MinLen = T1X;
		MaxLen = T2X;
		return true;
	}
	else
	{							//origin inside box
		MinLen = 0;
		MaxLen = T2X;
		return true;
	}
}

bool
RayPlaneCollisionTest(Vec3f PlaneN, Vec3f PlaneP, Vec3f RayOrigin, Vec3f RayDir, float &MaxLen)
{
	float d = -DOT(PlaneN, PlaneP);
	float t  = (-d -DOT(PlaneN, RayOrigin))/(DOT(PlaneN, RayDir));
	if(t >= 0 && t < MaxLen)
	{
		MaxLen = t;
		return true;
	}
	return false;
}
