#pragma once

enum class AttackType : uint32_t
{
	Bullet, Explosion, Melee
};

enum class WeaponType : uint32_t
{
	Shotgun, Pitchfork, Machete
};
