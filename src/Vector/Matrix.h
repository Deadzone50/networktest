#pragma once

#include <sstream>
#include "Vector.h"

struct Mat3f
{
	float m[9] = {	1,0,0,
					0,1,0,
					0,0,1};
	void SetCol(int C, Vec3f Col);
	void SetRow(int R, Vec3f Row);
	Vec3f GetCol(int C);
	Vec3f GetRow(int R);
	Mat3f Rotate(const Vec3f& R);
	Mat3f SetRotation(const Vec3f& R);
	Mat3f Rotate(const Vec3f &Axis, float Angle);
	void Identity();
};

struct Mat4f
{
	float m[16] = {	1,0,0,0,
					0,1,0,0,
					0,0,1,0,
					0,0,0,1};
	void SetCol(int C, Vec4f Col);
	void SetRow(int R, Vec4f Row);
	Vec4f GetCol(int C) const;
	Vec4f GetRow(int R) const;
	Mat4f Rotate(const Vec3f &R);
	Mat4f SetRotation(const Vec3f &R);
	Mat4f Rotate(const Vec3f &Axis, float Angle);
	Mat4f Translate(const Vec3f &T);
	Mat4f SetPosition(const Vec3f &T);
	Mat4f Scale(const Vec3f &V);
	//Mat3f GetXYZ();
	void Identity();
};

Mat4f LookAt(Vec3f CameraPos, Vec3f TargetPos, Vec3f Up);
Mat4f Perspective(float VerticalFOV, float AspectRatio, float NearField, float FarField);
Mat4f Ortho(float Left, float Right, float Bottom, float Top, float NearField, float FarField);
Mat4f INVERTreal(const Mat4f& M);
Mat4f INVERTaffine(const Mat4f& M);
Mat4f TRANSPOSE(const Mat4f& M);

Mat3f INVERTortho(const Mat3f& M);
Mat3f TRANSPOSE(const Mat3f& M);

///Mat4f
Mat4f operator*(const Mat4f& M1, const Mat4f& M2);
Vec4f operator*(const Mat4f& M, const Vec4f& V);
Mat4f operator*(float f, const Mat4f& M);			Mat4f operator*(const Mat4f& M, float f);

std::ostream& operator<<(std::ostream& os, const Mat4f& M);

///Mat3f
Mat3f operator*(const Mat3f& M1, const Mat3f& M2);
Vec3f operator*(const Mat3f& M, const Vec3f& V);
Mat3f operator*(float f, const Mat3f& M);			Mat3f operator*(const Mat3f& M, float f);

std::ostream& operator<<(std::ostream& os, const Mat3f& M);