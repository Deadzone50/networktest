#include "Vector.h"

#include <cmath>
#include <string>
#include <sstream>

bool
operator==(const Vec3f &V1, const Vec3f &V2)
{
	if(V1.x == V2.x)
		if(V1.y == V2.y)
			if(V1.z == V2.z)
				return true;
	return false;
}
Vec3f operator*(const Vec3f &V1, const Vec3f &V2)
{
	Vec3f V;
	V.x = V1.x*V2.x;
	V.y = V1.y*V2.y;
	V.z = V1.z*V2.z;
	return V;
}
Vec3f operator+(const Vec3f &V1, const Vec3f &V2)
{
	Vec3f V;
	V.x = V1.x+V2.x;
	V.y = V1.y+V2.y;
	V.z = V1.z+V2.z;
	return V;
}
Vec3f operator-(const Vec3f &V1, const Vec3f &V2)
{
	Vec3f V;
	V.x = V1.x-V2.x;
	V.y = V1.y-V2.y;
	V.z = V1.z-V2.z;
	return V;
}
void operator*=(Vec3f &V1, const Vec3f &V2)
{
	V1.x *= V2.x;
	V1.y *= V2.y;
	V1.z *= V2.z;
}
void operator+=(Vec3f &V1, const Vec3f &V2)
{
	V1.x += V2.x;
	V1.y += V2.y;
	V1.z += V2.z;
}
void operator-=(Vec3f &V1, const Vec3f &V2)
{
	V1.x -= V2.x;
	V1.y -= V2.y;
	V1.z -= V2.z;
}

Vec3f operator*(float f, const Vec3f &V1)
{
	Vec3f V;
	V.x=f*V1.x;
	V.y=f*V1.y;
	V.z=f*V1.z;
	return V;
}
Vec3f operator*(const Vec3f & V, float f)	{return f*V;}
Vec3f operator*(int i, const Vec3f &V1)
{
	Vec3f V;
	V.x=i*V1.x;
	V.y=i*V1.y;
	V.z=i*V1.z;
	return V;
}
Vec3f operator*(const Vec3f & V, int i)		{return i*V;}
Vec3f operator*(uint32_t i, const Vec3f &V1)
{
	Vec3f V;
	V.x=i*V1.x;
	V.y=i*V1.y;
	V.z=i*V1.z;
	return V;
}
Vec3f operator*(const Vec3f & V, uint32_t i){return i*V;}
Vec3f operator/(const Vec3f &V1, float f)
{
	Vec3f V;
	V.x=V1.x/f;
	V.y=V1.y/f;
	V.z=V1.z/f;
	return V;
}
Vec3f operator-(const Vec3f &V1)
{
	Vec3f V;
	V.x = -V1.x;
	V.y = -V1.y;
	V.z = -V1.z;
	return V;
}

std::ostream& operator<<(std::ostream &os, const Vec3f &V)
{
	os << V.x << " , " << V.y << " , " << V.z;
	return os;
}
std::istream& operator>>(std::istream &is, Vec3f &V)
{
	is >> V.x >> V.y >> V.z;
	return is;
}