#include "ParticleSystem.h"
#include "Random.h"

extern RandomGenerator RandGen;

void ParticleSystem::Initialize()
{
	m_Particles.reserve(256);

	glGenVertexArrays(1, &m_VAO);
	glGenBuffers(1, &m_VBO);
	//glGenBuffers(1, &m_EBO);

	glBindVertexArray(m_VAO);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	glBufferData(GL_ARRAY_BUFFER, 256 * sizeof(Particle), NULL, GL_STREAM_DRAW);

	//position
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), (void*)offsetof(Particle, Position));
	glEnableVertexAttribArray(0);

	//color
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), (void*)offsetof(Particle, Color));
	glEnableVertexAttribArray(1);

	//time
	glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, sizeof(Particle), (void*)offsetof(Particle, TimeLeft));
	glEnableVertexAttribArray(2);


	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_EBO);
	//glBufferData(GL_ELEMENT_ARRAY_BUFFER, 256 * sizeof(uint32_t), NULL, GL_STREAM_DRAW);

	m_Shader = Shader("Shader/particle.vert", "Shader/particle.frag");
}

void ParticleSystem::AddParticle(Vec3f Pos, Vec3f Col, Vec3f Speed, float Time)
{
	if(m_Particles.size() <= 256)
	{
		m_Particles.push_back({Pos, Col, Speed, Time});
	}
}

void ParticleSystem::Step(float dt)
{
	for(auto it = m_Particles.begin(); it != m_Particles.end(); ++it)
	{
		it->TimeLeft -= dt;
		if(it->TimeLeft < 0)
		{
			it = m_Particles.erase(it);
			if(it == m_Particles.end())
				break;
		}
		else
		{
			it->CurrentSpeed.y -= 9.81f*dt;
			it->Position += it->CurrentSpeed*dt;
		}
	}
}

void ParticleSystem::Draw(const Mat4f &View, const Mat4f &Projection)
{
	glBindVertexArray(m_VAO);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	glBufferSubData(GL_ARRAY_BUFFER, 0, m_Particles.size() * sizeof(Particle), m_Particles.data());

	m_Shader.Use();
	m_Shader.SetMat4("Projection", Projection);
	m_Shader.SetMat4("View", View);
	glDrawArrays(GL_POINTS, 0, m_Particles.size());
}
