#include "IO.h"

#include <iostream>
#include "Game.h"
#include "DebugDraws.h"

extern Game App;
extern DebugDraws DebugDrawer;
extern bool Wireframe;

void KeyDown(unsigned char Key, int MouseX, int MouseY)
{
	(void)MouseX;
	(void)MouseY;
	KeyStates[Key] = true;
	KeyChanged[Key] = true;
	
	if(App.GetState() == GameState::DisplayMenu)
	{
		if(Key == 22)		//ctrl-v
		{
			OpenClipboard(nullptr);
			HANDLE handle = GetClipboardData(CF_TEXT);
			char *text = (char*)GlobalLock(handle);
			std::string tmp(text);
			GlobalUnlock(handle);
			CloseClipboard();
			std::cout << tmp << "\n";
			App.GetCurrentMenu()->SetInput(tmp);
		}
		else
			App.GetCurrentMenu()->ChangeInput(Key);
	}
}
void KeyUp(unsigned char Key, int MouseX, int MouseY)
{	
	(void)MouseX;
	(void)MouseY;
	KeyStates[Key] = false;
	if('a' <= Key && Key <= 'z')									//97-122
		KeyStates[Key-32] = false;
	else if('A' <= Key && Key <= 'Z')								//65-90
		KeyStates[Key+32] = false;
}
void KeySpecialDown(int Key, int MouseX, int MouseY)
{
	(void)MouseX;
	(void)MouseY;
	KeySpecialStates[Key] = true;
}
void KeySpecialUp(int Key, int MouseX, int MouseY)
{
	(void)MouseX;
	(void)MouseY;
	KeySpecialStates[Key] = false;
}
void MouseFunc(int button, int state, int x, int y)
{
	(void)x;
	(void)y;
	MouseChanged[button] = true;
	if(button == 3 || button == 4)	//mousewheel
	{
		if(state == GLUT_DOWN)
			MouseButton[button] = true;
	}
	else
	{
		if(state == GLUT_DOWN)								//left,right, middle buttons
			MouseButton[button] = true;
		else
			MouseButton[button] = false;
	}
}
void MousePassiveMove(int x, int y)
{
	MousePos = {x, ScreenSize.y - y};
	MouseDelta.x = x - ScreenSize.x/2;
	MouseDelta.y = ScreenSize.y - y - ScreenSize.y/2;
}

bool MouseInit = true;
bool WarpMouse = true;

void ResetMouse()
{
	MouseButton[0] = false;
	MouseInit = true;
	WarpMouse = true;
	MouseDelta = {0,0};
}

void HideMouse()
{
	glutSetCursor(GLUT_CURSOR_NONE);
}

void ShowMouse()
{
	glutSetCursor(GLUT_CURSOR_LEFT_ARROW);
}

void HandleMouse(float dt)
{
	if(MouseInit)
	{
		MouseInit = false;
	}
	else
	{
		WarpMouse = false;
		if(MouseDelta.x || MouseDelta.y)
		{
			float MDx = (float)MouseDelta.x / 400;
			float MDy = (float)MouseDelta.y / 400;
			WarpMouse = true;
			Player->Rotate({MDy, MDx});
		}
	}

	if(WarpMouse)
	{
		glutWarpPointer(ScreenSize.x/2, ScreenSize.y/2);
	}
}

void HandleKeys(float dt)
{
	if(App.GetState() == GameState::DisplayMenu)
	{
		if(KeyChanged[27] && KeyStates[27])	//Esc
		{
			KeyChanged[27] = false;
			if(App.IsRunning())
				App.SwitchState(GameState::DisplayGame);
		}
	}
	else
	{
		float MoveSpeed_ = 5.0f;

		float Factor = 3.0f/2.0f;
		if(KeySpecialStates[GLUT_KEY_SHIFT])
		{
			Factor*=4;
		}
		if(KeyChanged[27] && KeyStates[27])	//Esc
		{
			KeyChanged[27] = false;
			App.SwitchState(GameState::DisplayMenu);
		}

		if(KeyStates['c'])
		{
			DebugDrawer.Clear();
		}
		if(KeyChanged['C'] && KeyStates['C'])
		{
			DebugDrawer.Enable();
			KeyChanged['C'] = false;
		}
		if(KeyChanged['x'] && KeyStates['x'])
		{
			Wireframe = !Wireframe;
			KeyChanged['x'] = false;
		}

		if(KeyStates['q'])
		{
			exit(0);
		}

		//Player
		if(Player->IsAlive())
		{
			if(KeyStates['w']||KeyStates['W'])
			{
				Player->Move(dt*MoveSpeed_*Factor*Vec3f{0,0,-1});
			}
			if(KeyStates['a']||KeyStates['A'])
			{
				Player->Move(dt*MoveSpeed_*Factor*Vec3f{-1,0,0});
			}
			if(KeyStates['s']||KeyStates['S'])
			{
				Player->Move(dt*MoveSpeed_*Factor*Vec3f{0,0,1});
			}
			if(KeyStates['d']||KeyStates['D'])
			{
				Player->Move(dt*MoveSpeed_*Factor*Vec3f{1,0,0});
			}
			if(KeyStates[' '])
			{
				Player->Jump();
			}
			if(MouseButton[0])
			{
				Player->Attack();
			}
			{
				//if(KeyChanged[' '] && KeyStates[' '])
				//{
				//	Player->Jump();
				//	KeyChanged[' '] = false;
				//}
				if(KeyChanged['z'] && KeyStates['z'])
				{
					Player->m_Debug = !Player->m_Debug;
					KeyChanged['z'] = false;
				}
				if(KeyChanged['e'] && KeyStates['e'])
				{
					Player->Interact();
					KeyChanged['e'] = false;
				}
				if(KeyChanged['1'] && KeyStates['1'])
				{
					Player->SwitchWeapon(1);
					KeyChanged['1'] = false;
				}
				if(KeyChanged['2'] && KeyStates['2'])
				{
					Player->SwitchWeapon(2);
					KeyChanged['2'] = false;
				}
			}
		}
	}
}
