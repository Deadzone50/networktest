#include "Vector/Matrix.h"
#include "Vertex.h"
#include "Collision.h"

bool operator==(const Vertex& lhs, const Vertex& rhs)
{
    return	((lhs.Position == rhs.Position) &&
			(lhs.Color == rhs.Color) &&
			(lhs.Normal == rhs.Normal) &&
			(lhs.TexCoord == rhs.TexCoord));
}

bool Mesh::RayCollision(Vec3f Origin, Vec3f Direction, float &Max)
{
	bool hit = false;
	for(uint32_t i = 0; i < Indices.size(); i+=3)
	{
		const Vertex &V1 = Vertices[Indices[i]];
		const Vertex &V2 = Vertices[Indices[i+1]];
		const Vertex &V3 = Vertices[Indices[i+2]];
		Vec3f VP[3] = {V1.Position, V2.Position, V3.Position};
		if(RayTriangleIntersection(Origin, Direction, VP, Max))
			hit = true;
	}
	return hit;
}

void Mesh::SetPos(const Vec3f &pos)
{
	Vec3f amount = pos - m_Pos;
	for(auto &V : Vertices)
	{
		V.Position += amount;
	}
}

void Mesh::Move(const Vec3f &amount)
{
	m_Pos += amount;
	for(auto &V : Vertices)
	{
		V.Position += amount;
	}
}

void Mesh::Rotate(const Vec3f &angles)
{
	Mat3f Rot;
	Rot.Rotate(angles);
	for(auto &V : Vertices)
	{
		V.Position = Rot*V.Position;
		V.Normal = Rot*V.Normal;
	}
}

void Mesh::Append(const Mesh &mesh)
{
	uint32_t past_last_index = Vertices.size();
	Indices.reserve(Indices.size() + mesh.Indices.size());
	for(auto &I : mesh.Indices)
	{
		Indices.push_back(I+past_last_index);
	}
	Vertices.insert(Vertices.end(), mesh.Vertices.begin(),  mesh.Vertices.end());
}

void CreateTextMesh(std::string Text, Vec3f Position, Vec3f Right, Vec3f Up, float Scale, 
					Vec3f Color, std::vector<Vertex> &Vertices, std::vector<uint32_t> &Indices)
{
	const uint8_t W = 9;
	const uint8_t H = 9;
	const uint8_t P = 1;

	float WS = (W-P)*Scale;
	float HS = H*Scale;
	float PS = P*Scale;

	const uint32_t ImageW = 321;
	const uint32_t ImageH = 31;

	uint32_t index = Vertices.size();

	Vec3f Pos = Position;
	Vertex V[4];
	V[0].Color = Color;
	V[1].Color = Color;
	V[2].Color = Color;
	V[3].Color = Color;
	for(const auto &C : Text)
	{
		uint8_t Row = (C - 32)/32;
		uint8_t Col = (C - 32)%32;
		float y0 = Row*(H+P) + P + 0.5f;
		float y1 = y0+H-P;

		float x0 = Col*(W+P) + P + 0.5f;
		float x1 = x0+H-P;

		y0/=ImageH;
		y1/=ImageH;
		x0/=ImageW;
		x1/=ImageW;

		V[0].Position = Pos;
		V[1].Position = Pos + Right*WS;
		V[2].Position = Pos + Right*WS + Up*HS;
		V[3].Position = Pos + Up*HS;

		V[0].TexCoord = {x0, y0};
		V[1].TexCoord = {x1, y0};
		V[2].TexCoord = {x1, y1};
		V[3].TexCoord = {x0, y1};

		Vertices.push_back(V[0]);
		Vertices.push_back(V[1]);
		Vertices.push_back(V[2]);
		Vertices.push_back(V[3]);

		Indices.push_back(index++);
		Indices.push_back(index++);
		Indices.push_back(index++);
		Indices.push_back(index++);

		Pos += Right*WS;
	}
}
