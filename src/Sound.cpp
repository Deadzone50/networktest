#include <AL/al.h>
#include <AL/alc.h>

#include "Sound.h"

#include <fstream>
#include <iostream>

#include "Game.h"

extern Game App;

void SoundManager::Initialize()
{
	ALCdevice *device;
	device = alcOpenDevice(nullptr);
	if(!device)
	{
		std::cout << "Failed to initialize sound\n";
		abort();
	}

	ALCcontext *context = alcCreateContext(device, NULL);
	if(!context)
	{
		std::cerr << "Failed to create audio context\n";
		abort();
	}
	if(!alcMakeContextCurrent(context))
	{
		std::cout << "Failed to set context\n";
		abort();
	}
}

void SoundManager::LoadSoundFile(const std::string &Name)
{
	uint32_t Channels, SampleRate, BitsPerSample;
	std::vector<char> SoundData;

	LoadWavFile("Assets/Sound/"+Name+".wav", Channels, SampleRate, BitsPerSample, SoundData);

	ALenum format;
	if(Channels == 1 && BitsPerSample == 8)
		format = AL_FORMAT_MONO8;
	else if(Channels == 1 && BitsPerSample == 16)
		format = AL_FORMAT_MONO16;
	else if(Channels == 2 && BitsPerSample == 8)
		format = AL_FORMAT_STEREO8;
	else if(Channels == 2 && BitsPerSample == 16)
		format = AL_FORMAT_STEREO16;
	else
	{
		std::cout << "Invalid audio format\n";
		abort();
	}
	uint32_t Buffer;
	alGenBuffers(1, &Buffer);
	alBufferData(Buffer, format, SoundData.data(), SoundData.size(), SampleRate);
	m_SoundMap[Name] = Buffer;
}

void SoundManager::Play(const std::string &Name, Vec3f Position, float Volume, float Pitch)
{

	Vec3f SoundDir = Position - App.GetPlayers()[0]->GetPosition();
	Vec2f PFacingDir = App.GetPlayers()[0]->GetFacingDirection();
	Mat3f R;
	float cosangle = DOT(Vec2f{0,-1}, PFacingDir);
	float angle = acosf(cosangle);
	if(DOT(Vec2f{1,0}, PFacingDir) > 0)
		angle = -angle;
	R.SetRotation({0, angle, 0});
	Vec3f SoundPos = R*SoundDir;

	uint32_t Source;
	alGenSources(1, &Source);
	alSourcef(Source, AL_PITCH, Pitch);
	alSourcef(Source, AL_GAIN, m_Volume*Volume);
	alSource3f(Source, AL_POSITION, SoundPos.x, SoundPos.y, SoundPos.z);		//Default listener at 0,0,0 looking to -Z
	alSource3f(Source, AL_VELOCITY, 0,0,0);
	alSourcei(Source, AL_LOOPING, AL_FALSE);
	alSourcei(Source, AL_BUFFER, m_SoundMap[Name]);

	alSourcePlay(Source);
	auto it = m_Sources.begin();
	while(true)		//check if sounds have finished playing
	{
		if(it == m_Sources.end())
			break;
		ALint state;
		alGetSourcei(*it, AL_SOURCE_STATE, &state);
		if(state != AL_PLAYING)
		{
			it = m_Sources.erase(it);
		}
		else
		{
			++it;
		}
	}

	m_Sources.push_back(Source);
}

bool LoadWavFile(const std::string &Name, uint32_t &Channels, uint32_t &SampleRate, uint32_t &BitsPerSample,
				 std::vector<char> &SoundData)
{
	std::ifstream file(Name, std::ios::binary);
	if(!file.is_open())
	{
		std::cout << "Cannot open " << Name << "\n";
		return false;
	}
	char Buffer[4] = {};
	file.read(Buffer, 4);				//ChunkID
	if(std::strncmp(Buffer, "RIFF", 4) != 0)
	{
		std::cout << "Not RIFF\n";
		return false;
	}
	file.ignore(4);						//Chunksize
	file.read(Buffer, 4);				//Format
	if(std::strncmp(Buffer, "WAVE", 4) != 0)
	{
		std::cout << "Not WAVE\n";
		return false;
	}
	file.read(Buffer, 4);				//SubchunkID
	if(std::strncmp(Buffer, "fmt ", 4) != 0)
	{
		std::cout << "Not fmt\n";
		return false;
	}
	file.ignore(6);						//SubchunkSize + AudioFormat
	file.read(Buffer, 2);				//NumChannels
	Channels = 0;
	std::memcpy(&Channels, &Buffer[0], 2);

	file.read(Buffer, 4);				//SampleRate
	SampleRate = 0;
	std::memcpy(&SampleRate, &Buffer[0], 4);
	file.ignore(4+2);					//ByteRate + BlockAlign
	file.read(Buffer, 2);				//BitsPerSample
	BitsPerSample = 0;
	std::memcpy(&BitsPerSample, &Buffer[0], 2);

	file.read(Buffer, 4);				//SubchunkID
	if(std::strncmp(Buffer, "data", 4) != 0)
	{
		std::cout << "Not data\n";
		return false;
	}
	file.read(Buffer, 4);				//SubchunkSize
	uint32_t Size = 0;
	std::memcpy(&Size, &Buffer[0], 4);
	SoundData.resize(Size);
	file.read(SoundData.data(), Size);	//Data

	return true;
}
