#pragma once
#include <random>
#include "Vector/Vector.h"

class RandomGenerator
{
public:
	RandomGenerator();
	RandomGenerator(uint32_t Seed);

	float GetFloat(float Min, float Max);
	Vec2f GetV2f(float Min, float Max);
	Vec3f GetV3f(float Min, float Max);
private:
	std::mt19937 m_Generator;
};
