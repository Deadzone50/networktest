#include "DebugDraws.h"

void DebugDraws::Initialize()
{
	glGenVertexArrays(1, &m_VAOP);
	glGenVertexArrays(1, &m_VAOL);
	glGenBuffers(1, &m_VBOP);
	glGenBuffers(1, &m_VBOL);
	glGenBuffers(1, &m_EBOP);
	glGenBuffers(1, &m_EBOL);

	glBindVertexArray(m_VAOP);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOP);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vec3f)*512, NULL, GL_DYNAMIC_DRAW);
	// position
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 2*sizeof(Vec3f), (void*)0);
	glEnableVertexAttribArray(0);
	// color
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 2*sizeof(Vec3f), (void*)sizeof(Vec3f));
	glEnableVertexAttribArray(1);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_EBOP);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint32_t)*512, NULL, GL_DYNAMIC_DRAW);

	glBindVertexArray(m_VAOL);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOL);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vec3f)*512, NULL, GL_DYNAMIC_DRAW);
	// position
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 2*sizeof(Vec3f), (void*)0);
	glEnableVertexAttribArray(0);
	// color
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 2*sizeof(Vec3f), (void*)sizeof(Vec3f));
	glEnableVertexAttribArray(1);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_EBOL);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint32_t)*512, NULL, GL_DYNAMIC_DRAW);
}

void DebugDraws::UpdateBuffers()
{
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOP);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(Vec3f)*m_VerticesPoints.size(), m_VerticesPoints.data());
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_EBOP);
	glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, sizeof(uint32_t)*m_IndicesPoints.size(), m_IndicesPoints.data());

	glBindBuffer(GL_ARRAY_BUFFER, m_VBOL);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(Vec3f)*m_VerticesLines.size(), m_VerticesLines.data());
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_EBOL);
	glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, sizeof(uint32_t)*m_IndicesLines.size(), m_IndicesLines.data());
}

void DebugDraws::DrawPoint(Vec3f P, Vec3f Color)
{
	if(m_IndicesPoints.size() > 512)
	{
		Clear();
	}

	m_VerticesPoints.push_back(P);
	m_VerticesPoints.push_back(Color);

	uint32_t size = m_IndicesPoints.size();
	m_IndicesPoints.push_back(size);
	m_Update = true;
}

void DebugDraws::DrawLine(Vec3f S, Vec3f E, Vec3f Color)
{
	if(m_IndicesLines.size() > 512)
	{
		Clear();
	}
	m_VerticesLines.push_back(S);
	m_VerticesLines.push_back(Color);
	m_VerticesLines.push_back(E);
	m_VerticesLines.push_back(Color);

	uint32_t size = m_IndicesLines.size();
	m_IndicesLines.push_back(size);
	m_IndicesLines.push_back(size+1);
	m_Update = true;
}

void DebugDraws::DrawTriangle(Vec3f V1, Vec3f V2, Vec3f V3, Vec3f Color)
{
}

void DebugDraws::DrawDebugGraphics(Shader &Shader)
{
	if(m_Enabled)
	{
		if(m_Update)
		{
			UpdateBuffers();
			m_Update = false;
		}

		Shader.Use();
		glBindVertexArray(m_VAOP);
		glDrawElements(GL_POINTS, m_IndicesPoints.size(), GL_UNSIGNED_INT, (void*)0);

		glBindVertexArray(m_VAOL);
		glDrawElements(GL_LINES, m_IndicesLines.size(), GL_UNSIGNED_INT, (void*)0);
	}
}

void DebugDraws::Clear()
{
	m_IndicesPoints.clear();
	m_IndicesLines.clear();
	m_VerticesLines.clear();
	m_VerticesPoints.clear();
}
