#pragma once
#include <vector>
#include <functional>
#include "Vertex.h"
#include "Enemy.h"
#include "Weapon.h"

class PlayerClass;

enum class ItemType
{
	Ammo, Health
};

class Item
{
public:
	Item(ItemType Type, Vec3f Pos);
	ItemType GetType() { return m_Type; }
	Vec3f GetPosition() { return m_Pos; }

	void Draw(Shader &Shader);

	uint32_t GetId() {return m_Id;}
private:
	Vec3f m_Pos;
	ItemType m_Type;

	uint32_t m_TextureId, m_VBO, m_TextureRow, m_TextureCol, m_Id;
};

class MapObject
{
public:
	void SetMesh(Mesh *mesh);			//Only updates collision box
	bool RayCollision(Vec3f Origin, Vec3f Direction, float &Max);
	Vec2V3 GetAABB();
private:
	Vec3f m_AABB[2];
};

class Interactable
{
public:
	void SetFunction(std::function<void()> OnUse) { m_OnUse = OnUse; }
	void Use() { m_OnUse(); }
private:
	std::function<void()> m_OnUse;
};

class Entrance: public MapObject
{

};

class Exit : public Interactable, public MapObject
{

};

class Map
{
public:
	void Initialize();
	void Reset();
	void GenerateLevel();
	void LoadNextLevel();

	void Draw(Shader &Shader, bool Wireframe) const;

	std::vector<Mesh*> &GetMeshes() { return m_Meshes; }
	std::vector<EnemyClass> &GetEnemies() { return m_Enemies; }
	std::vector<Item> &GetItems() { return m_Items; }

	Vec2V3 HighLight(Vec3f Origin, Vec3f Dir, float &MaxRange);
	bool Interact(Vec3f Origin, Vec3f Dir, float &MaxRange);
	bool Attack(Vec3f Origin, Vec3f Dir, float Damage, AttackType Type, float &MaxRange);

	void SetState(uint32_t Id, CharacterState State);
private:
	std::vector<Mesh*> m_Meshes;

	Entrance m_Entrance;
	Exit m_Exit;
	std::vector<Mesh> m_LayoutMeshes;

	std::vector<EnemyClass> m_Enemies;
	std::vector<Item> m_Items;

	uint32_t m_VAO, m_VBO, m_EBO;
};

