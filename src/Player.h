#pragma once
#include <vector>
#include "Vector/Matrix.h"
#include "Vector/Vector.h"
#include "Shader.h"
#include "Character.h"
#include "Vertex.h"
#include "Weapon.h"

class Map;

class PlayerClass : public Character
{
public:
	PlayerClass();

	void SetRotation(Vec2f Angles);
	void Rotate(Vec2f Angles);
	Mat4f GetViewMatrix();

	void Move(const Vec3f &Amount);

	void Jump();
	void Interact();

	void Step(float dt);

	void AddText(const std::string &Text, const Vec3f &Position, const Vec3f &Color);
	void ClearText();
	void UpdateUI();
	void DrawUI(const Mat4f &ProjectionMatrix);
	void Attack();

	void ChangeHealth(float Amount);
	void ChangeAmmo(int32_t Amount);
	//bool RayCollision(Vec3f Origin, Vec3f Direction, float &Max);

	uint32_t GetAmmo() const { return m_Ammo; }
	float GetHealth() const { return m_Health; }

	void SwitchWeapon(uint8_t Number);

	void Reset();

	bool m_Debug = false;
private:
	Vec2f m_Rotation;
	Vec3f m_LookingDirection, m_Right, m_Up;

	uint32_t m_UITextureId;
	uint32_t m_VAO, m_VBO, m_EBO;
	Mat4f m_UIProjectionMatrix;
	Shader m_Shader, m_TextShader;
	uint32_t m_AnimFrame = 0;
	float m_FrameTime = 0.05f;
	float m_AttackTime = 0.8f;
	float m_Timer = 0;

	uint32_t m_VAOText, m_VBOText, m_EBOText;
	std::vector<Vertex> m_Vertices;
	std::vector<uint32_t> m_Indices;

	std::vector<std::string> m_Text;
	std::vector<Vec3f> m_TextPos;
	std::vector<Vec3f> m_TextCol;

	WeaponType m_CurrentWeapon = WeaponType::Shotgun;
	uint32_t m_AnimRow = 3;
};
