#pragma once
#include "Vector/Vector.h"

int RayTriangleIntersection(Vec3f Origin, Vec3f Direction, Vec3f Triangle[3], float &tmax);

bool RayAABBCollisionTest(Vec3f Origin, Vec3f Direction, Vec3f AABB[2], float& MinLen, float& MaxLen);

bool RayPlaneCollisionTest(Vec3f PlaneN, Vec3f PlaneP, Vec3f RayOrigin, Vec3f RayDir, float &MaxLen);
