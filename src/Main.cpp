#include <GL/glew.h>
#include <GL/glut.h>

#include <iostream>
#include <vector>
#include <list>
#include <chrono>

#include "Vector/Vector.h"
#include "Vector/Matrix.h"
#include "IO.h"
#include "Network.h"
#include "Shader.h"
#include "Map.h"
#include "TextureManager.h"
#include "Random.h"
#include "DebugDraws.h"
#include "Game.h"
#include "Sound.h"

TextureManager TexManager;
SoundManager AudioManager;
DebugDraws DebugDrawer;
RandomGenerator RandGen;

bool KeyStates[256] = {false};
bool KeySpecialStates[256] = {false};
bool KeyChanged[256] = {false};

bool InitMouse = false;
Vec2i MousePos, MouseDelta;
Vec3f MousePos3d, MouseDir;
bool MouseButton[5]{false};
bool MouseChanged[5] = {false};

bool Wireframe = false;

bool Multiplayer = false;
Socket *Sock;
std::list<std::string> MessageLog;
bool MessagesChanged = false;

Vec2i ScreenSize;
PlayerClass *Player;		//The controlled player

Game App;

void Reshape(int width, int height)
{
	ScreenSize = {width, height};
	glViewport(0, 0, width, height);
	App.GetCurrentMenu()->Update(width, height);
}

uint32_t VAOBillboard, VBOBillboard, EBOBillboard;
uint32_t NumElements1;
Shader NormalShader, BillboardShader;
unsigned int ModelLocation, ViewLocation, ProjectionLocation;
Mat4f ProjectionMatrix, ViewMatrix, ModelMatrix;

bool InitResources()
{
	AudioManager.Initialize();
	AudioManager.LoadSoundFile("Shotgun");
	AudioManager.LoadSoundFile("Shotgun2");

	AudioManager.LoadSoundFile("Stab-01");
	AudioManager.LoadSoundFile("Stab-02");
	AudioManager.LoadSoundFile("Stab-Hit");
	AudioManager.LoadSoundFile("Swing");

	AudioManager.LoadSoundFile("Pickup-Ammo");
	AudioManager.LoadSoundFile("Pickup-Health");

	AudioManager.LoadSoundFile("Death");
	AudioManager.LoadSoundFile("Pain");

	NormalShader = Shader("Shader/shader.vert", "Shader/shader.frag");
	BillboardShader = Shader("Shader/billboardshader.vert", "Shader/billboardshader.frag");

	NormalShader.Use();		//draw scene
	NormalShader.SetMat4("Projection", ProjectionMatrix);

	BillboardShader.Use();
	BillboardShader.SetSampler("Texture", 0);
	BillboardShader.SetMat4("Projection", ProjectionMatrix);

	Player = new PlayerClass();

	App.AddPlayer(Player);
	App.InitGame();

	DebugDrawer.Initialize();

	/// Billboards
	Vertex Quad[4];
	Quad[0].Position = {-1,-1, 0};
	Quad[1].Position = {1,-1, 0};
	Quad[2].Position = {1,1, 0};
	Quad[3].Position = {-1,1, 0};

	Quad[0].Normal = {0,0,1};
	Quad[1].Normal = {0,0,1};
	Quad[2].Normal = {0,0,1};
	Quad[3].Normal = {0,0,1};

	Quad[0].TexCoord = {0,0};
	Quad[1].TexCoord = {1,0};
	Quad[2].TexCoord = {1,1};
	Quad[3].TexCoord = {0,1};

	uint32_t Indices[4] = 
	{
		0, 1, 2, 3
	};

	glGenVertexArrays(1, &VAOBillboard);
	glGenBuffers(1, &VBOBillboard);
	glGenBuffers(1, &EBOBillboard);

	glBindVertexArray(VAOBillboard);
	glBindBuffer(GL_ARRAY_BUFFER, VBOBillboard);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex)*4, &Quad[0], GL_STATIC_DRAW);
	// position
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Position));
	glEnableVertexAttribArray(0);
	// texture
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, TexCoord));
	glEnableVertexAttribArray(1);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBOBillboard);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(Indices), &Indices[0], GL_STATIC_DRAW);


	glPointSize(5);
	glEnable(GL_POINT_SPRITE);

	uint32_t FontId = TexManager.Texture("Assets/Font9x9.png");
	
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glActiveTexture(GL_TEXTURE1);				//activate texture unit 1
	glBindTexture(GL_TEXTURE_2D, FontId);		//bind font texture to texture unit 1

	glActiveTexture(GL_TEXTURE0);				//activate texture unit 0
	return true;
}

float Time = 0;
uint8_t Frame = 0;
auto t0Render = std::chrono::high_resolution_clock::now();
auto t0Network = std::chrono::high_resolution_clock::now();
auto t0Game = std::chrono::high_resolution_clock::now();
void MainLoop()
{
	auto now = std::chrono::high_resolution_clock::now();
	float Renderdt = std::chrono::duration<float>(now - t0Render).count();
	float Networkdt = std::chrono::duration<float>(now - t0Network).count();
	float Gamedt = std::chrono::duration<float>(now - t0Game).count();

	if(Gamedt*1000 > 8)
	{
		t0Game = std::chrono::high_resolution_clock::now();
		HandleKeys(Gamedt);
		if(App.GetState() == GameState::DisplayGame)
		{
			HandleMouse(Gamedt);
			Player->Step(Gamedt);
			for(auto &E : App.GetCurrentLevel().GetEnemies())
			{
				E.Step(Gamedt);
			}
		}
		App.Step(Gamedt);
	}

	if(Multiplayer && (Networkdt*1000 > 16))		//16ms network tick, ~60fps
	{
		t0Network = std::chrono::high_resolution_clock::now();
		if(App.GetGameType() == GameType::Client)
		{
			ClientSync(Sock);
		}
		else
		{
			HostSync(Sock);
		}

		std::vector<GameEvent> Events = App.GetEvents();
		App.ClearEvents();
		for(auto &E : Events)
		{
			App.ProcessEvent(E);
		}
	}

	if(Renderdt*1000 > 4)		//slow down reneding 4ms minimum
	{
		if(!Multiplayer)
		{
			std::vector<GameEvent> Events = App.GetEvents();
			App.ClearEvents();
			for(auto &E : Events)
			{
				App.ProcessEvent(E);
			}
		}

		t0Render = std::chrono::high_resolution_clock::now();
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		if(App.GetState() == GameState::DisplayGame)
		{
			NormalShader.Use();		//draw scene
			ModelMatrix.Identity();
			NormalShader.SetMat4("Model", ModelMatrix);

			ViewMatrix = Player->GetViewMatrix();
			NormalShader.SetMat4("View", ViewMatrix);
			NormalShader.SetVec3("CameraPos", Player->GetPosition());

			//glBindVertexArray(VAO1);
			//glDrawElements(GL_TRIANGLES, NumElements1, GL_UNSIGNED_INT, (void*)0);
			App.GetCurrentLevel().Draw(NormalShader, Wireframe);
			App.DrawUI(NormalShader);

			//Billboards
			glBindVertexArray(VAOBillboard);

			BillboardShader.Use();
			BillboardShader.SetMat4("View", ViewMatrix);
			BillboardShader.SetVec3("CameraPos", Player->GetPosition());
			BillboardShader.SetFloat("Scale", 1);			//size of sprite in game
			BillboardShader.SetFloat("TextureScale", 0.1f);	//size of one sprite in texture

			for(auto &E : App.GetCurrentLevel().GetEnemies())
			{
				//E.Step(dt);
				E.Draw(BillboardShader, Player->GetPosition());
			}
			auto &Players = App.GetPlayers();
			for(auto P = Players.begin(); P != Players.end(); ++P)
			{
				if((*P)->GetId() != Player->GetId())
				{
					(*P)->Draw(BillboardShader, Player->GetPosition());
				}
			}
			BillboardShader.SetFloat("Scale", 0.2f);
			App.DrawItems(BillboardShader);

			//App.Step(dt);
			App.Draw(ViewMatrix, ProjectionMatrix);

			glClear(GL_DEPTH_BUFFER_BIT);
			Player->DrawUI(ProjectionMatrix);
		}
		else if(App.GetState() == GameState::DisplayMenu)
		{
			App.GetCurrentMenu()->Draw();
			if(MouseButton[0] && MouseChanged[0])
			{
				App.GetCurrentMenu()->Click(MousePos);
				MouseChanged[0] = false;
			}
		}
		glClear(GL_DEPTH_BUFFER_BIT);
		DebugDrawer.DrawDebugGraphics(NormalShader);

		glutSwapBuffers();
		if(App.GetState() == GameState::DisplayGame)
		if(!(++Frame%100))	//every 100th frame
		{
			std::cout << "ms: " << Renderdt*1000;
			std::cout << "\tFPS: " << 1.0f/Renderdt;
			std::cout << "\tPOS: " << Player->GetPosition();
			std::cout << "\tMPos: " << MousePos;
			std::cout << "\tMDelta: " << MouseDelta << "\n";
			Frame = 0;
		}
	}
}

int PauseExit(int Code)
{
	std::cin.ignore();
	return Code;
}

int main(int argc, char **argv)
{
	ScreenSize = Vec2i{800,600};

	ProjectionMatrix = Perspective(90.0f, (float)ScreenSize.x/ScreenSize.y, 0.1f, 100.0f);
	if(!InitializeSockets())
	{
		std::cout << "Error: socket init failed\n";
		return PauseExit(EXIT_FAILURE);
	}

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowSize(ScreenSize.x, ScreenSize.y);
	glutInitWindowPosition(100,20);
	glutCreateWindow("FPS");

	glutDisplayFunc(MainLoop);
	glutReshapeFunc(Reshape);
	glutIdleFunc(MainLoop);

	glutKeyboardFunc(KeyDown);			glutKeyboardUpFunc(KeyUp);
	glutSpecialFunc(KeySpecialDown);	glutSpecialUpFunc(KeySpecialUp);
	glutMouseFunc(MouseFunc);
	glutMotionFunc(MousePassiveMove);
	glutPassiveMotionFunc(MousePassiveMove);
	glutIgnoreKeyRepeat(1);

	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_FRAMEBUFFER_SRGB);	//gamma correction
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//glDepthFunc(GL_LESS);

	GLenum GlewStatus = glewInit();
	if(GlewStatus != GLEW_OK)
	{
		std::cout << "Error glewInit: " << glewGetErrorString(GlewStatus) << std::endl;
		return PauseExit(EXIT_FAILURE);
	}
	if (!GLEW_VERSION_3_3)
	{
		std::cout << "Error: your graphic card does not support OpenGL 3.3\n";
		return PauseExit(EXIT_FAILURE);
	}

	if (!InitResources())
	{
		std::cout << "Error: InitResources failed\n";
		return PauseExit(EXIT_FAILURE);
	}

	t0Render = std::chrono::high_resolution_clock::now();
	t0Network = std::chrono::high_resolution_clock::now();
	t0Game = std::chrono::high_resolution_clock::now();
	glutMainLoop();

	return EXIT_SUCCESS;
}
