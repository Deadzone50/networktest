#include <chrono>
#include "Random.h"

RandomGenerator::RandomGenerator()
{
	uint32_t seed = std::chrono::system_clock::now().time_since_epoch().count();
	m_Generator.seed(seed);
}

RandomGenerator::RandomGenerator(uint32_t Seed)
{
	m_Generator.seed(Seed);
}

float RandomGenerator::GetFloat(float Min, float Max)
{
	return ((float)m_Generator() / m_Generator.max()) * (Max-Min) + Min;
}


Vec2f RandomGenerator::GetV2f(float Min, float Max)
{
	Vec2f V;
	V.x = ((float)m_Generator() / m_Generator.max()) * (Max-Min) + Min;
	V.y = ((float)m_Generator() / m_Generator.max()) * (Max-Min) + Min;
	return V;
}


Vec3f RandomGenerator::GetV3f(float Min, float Max)
{
	Vec3f V;
	V.x = ((float)m_Generator() / m_Generator.max()) * (Max-Min) + Min;
	V.y = ((float)m_Generator() / m_Generator.max()) * (Max-Min) + Min;
	V.z = ((float)m_Generator() / m_Generator.max()) * (Max-Min) + Min;
	return V;
}
