#include "Game.h"

#include <iostream>
#include <list>
#include "Network.h"
#include "Random.h"
#include "State.h"
#include "IO.h"
#include "Sound.h"

extern SoundManager AudioManager;

extern GameState CurrentState;

extern Mat4f ProjectionMatrix;
extern Vec2i ScreenSize;

extern bool MouseButton[5];
extern bool MouseInit;
extern bool WarpMouse;

extern RandomGenerator RandGen;

extern bool Multiplayer;
extern Socket *Sock;
extern std::list<std::string> MessageLog;

std::string ToSend;
std::string RecvPort = "30001";
extern Address SendAddr;

extern PlayerClass* Player;

Address CreateAddress(std::string IP, std::string Port)
{
	int a, b, c, d;
	char tmp;
	std::stringstream ss(IP);
	ss >> a >> tmp >> b >> tmp >> c >> tmp >> d;

	Address Addr(a, b, c, d, std::stoi(Port));
	return Addr;
}

void Game::InitMenus()
{
	//uint32_t FontId = TexManager.Texture("Assets/Font9x9.png");

	Menu *MainMenu = new Menu();
	MainMenu->AddButton("Start", [&]{SwitchState(GameState::DisplayGame);});
	MainMenu->AddButton("Multiplayer", [&]{SwitchState(GameState::DisplayMultiplayer);});
	MainMenu->AddButton("Settings", [&]{SwitchState(GameState::DisplaySettings);});
	MainMenu->AddButton("Exit", [&]{Exit();});

	Menu *MultiplayerMenu = new Menu();
	MultiplayerMenu->AddInput("IP", &m_Other_IP);
	MultiplayerMenu->AddInput("PORT", &m_Port);
	MultiplayerMenu->AddInput("RECV PORT", &RecvPort);

	MultiplayerMenu->AddButton("Host", [&]
	{
		m_Type = GameType::Host;
		m_Port = "30001";
		RecvPort = "30000";
		//Multiplayer = true;
	});
	MultiplayerMenu->AddButton("Client", [&]
	{
		m_Type = GameType::Client;
		m_Port = "30000";
		RecvPort = "30001";
		//Multiplayer = true;
	});

	MultiplayerMenu->AddButton("Connect", [&]
	{
		if(!Multiplayer)
		{
			SendAddr = CreateAddress(m_Other_IP, m_Port);
			std::cout	<< "Connecting to: " << (int)SendAddr.GetA() << ":" << (int)SendAddr.GetB()
						<< ":" << (int)SendAddr.GetC() << ":" << (int)SendAddr.GetD() << "\n";
			Sock = new Socket();
			Sock->Open(std::stoi(RecvPort));
			if(EstablishConnection(Sock, SendAddr))
			{
				std::cout << "Connection established\n";
				Multiplayer = true;
				PlayerClass *Other = new PlayerClass();
				AddPlayer(Other);
				if(m_Type == GameType::Client)
				{
					Player = Other;
				}
				m_Level.Reset();
			}
			else
			{
				Sock->Close();
				std::cout << "Connection timed out\n";
			}
		}
	});
	MultiplayerMenu->AddButton("Close connection", [&]
	{
		if(Multiplayer)
		{
			Multiplayer = false;
			Sock->Close();
			delete Sock;
			std::cout << "Socket closed\n";
		}
	});
	/*MultiplayerMenu->AddInput("Msg", &ToSend);
	MultiplayerMenu->AddButton("Send", [&]
	{
		if(!ToSend.empty())
		{
			std::cout << "Sending: " << ToSend << "\n";
			if(Sock->Send(SendAddr, ToSend.data(), ToSend.size()))
			{
				ToSend.clear();
			}
			else
			{
				MessageLog.push_back("Send failed\n");
			}
		}
	});*/
	MultiplayerMenu->AddButton("Back", [&]{SwitchState(GameState::DisplayMenu);});

	Menu *SettingsMenu = new Menu();
	SettingsMenu->AddButton("VOL-", [&]
	{
		float vol = AudioManager.GetVolume() - 0.1f;
		if(vol < 0)
			vol = 0;
		AudioManager.SetVolume(vol);
		AudioManager.Play("Shotgun2", m_Players[0]->GetPosition());
		std::cout << "VOL: " << vol << "\n";
	});
	SettingsMenu->AddButton("VOL+", [&]
	{
		float vol = AudioManager.GetVolume() + 0.1f;
		if(vol > 2)
			vol = 2;
		AudioManager.SetVolume(vol);
		AudioManager.Play("Shotgun2", m_Players[0]->GetPosition());
		std::cout << "VOL: " << vol << "\n";
	});

	SettingsMenu->AddButton("Back", [&]{SwitchState(GameState::DisplayMenu);});

	m_Menus.push_back(MainMenu);
	m_Menus.push_back(MultiplayerMenu);
	m_Menus.push_back(SettingsMenu);

	m_CurrentMenu = MainMenu;
}

void Game::InitGame()
{
	InitMenus();
	m_CurrentState = GameState::DisplayMenu;
	m_CurrentMenu = m_Menus[0];

	m_Level.Initialize();
	m_Level.GenerateLevel();
	m_ParticleSystem.Initialize();

	glGenVertexArrays(1, &m_VAO);		//For points
	glGenBuffers(1, &m_VBO);
	glGenBuffers(1, &m_EBO);

	glBindVertexArray(m_VAO);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	glBufferData(GL_ARRAY_BUFFER, 256 * 2*sizeof(Vec3f), NULL, GL_STREAM_DRAW);

	//position
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 2*sizeof(Vec3f), (void*)0);
	glEnableVertexAttribArray(0);

	//color
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 2*sizeof(Vec3f), (void*)sizeof(Vec3f));
	glEnableVertexAttribArray(1);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 256 * sizeof(uint32_t), NULL, GL_STREAM_DRAW);

}

void Game::SwitchState(GameState State)
{
	if(State == GameState::DisplayGame)
	{
		m_Started = true;
		m_CurrentState = GameState::DisplayGame;
		//ProjectionMatrix = Perspective(90, (float)ScreenSize.x/ScreenSize.y, 0.01f, 1000.0f);
		m_Menus[0]->ChangeButton(0, "Restart", [&]{m_Level.GenerateLevel(); SwitchState(GameState::DisplayGame);});
		ResetMouse();
		HideMouse();
	}
	else if (State == GameState::DisplayMenu)
	{
		m_CurrentState = GameState::DisplayMenu;
		m_CurrentMenu = m_Menus[0];
		m_CurrentMenu->Reset();
		ResetMouse();
		ShowMouse();
	}
	else if (State == GameState::DisplayMultiplayer)
	{
		m_CurrentState = GameState::DisplayMenu;
		m_CurrentMenu = m_Menus[1];
		m_CurrentMenu->Reset();
		ResetMouse();
		ShowMouse();
	}
	else if (State == GameState::DisplaySettings)
	{
		m_CurrentState = GameState::DisplayMenu;
		m_CurrentMenu = m_Menus[2];
		m_CurrentMenu->Reset();
		ResetMouse();
		ShowMouse();
	}
}

void Game::Exit()
{
	exit(0);
}

void Game::Interact(Vec3f Origin, Vec3f Dir, float Max)
{
	m_Level.Interact(Origin, Dir, Max);
}

void Game::Attack(Vec3f Origin, Vec3f Dir, float Damage, AttackType Type, float MaxRange)
{
	float t = MaxRange;
	m_Level.Attack(Origin, Dir, Damage, Type, t);
}

void Game::Draw(const Mat4f &View, const Mat4f &Projection)
{
	m_ParticleSystem.Draw(View, Projection);
}

void Game::DrawItems(Shader &Shader)
{
	for(auto &I : m_Level.GetItems())
	{
		I.Draw(Shader);
	}
}

void Game::AddParticle(Vec3f Pos, Vec3f Col, Vec3f Speed, float Time)
{
	m_ParticleSystem.AddParticle(Pos, Col, Speed, Time);
}

void Game::Step(float dt)
{
	m_ParticleSystem.Step(dt);
	if(m_Type == GameType::Host)
	{
		std::vector<Item> &Items =  m_Level.GetItems();
		for(auto I = Items.begin(); I != Items.end();)
		{
			Vec3f IPos = I->GetPosition();
			Vec2f IPos2D = {IPos.x, IPos.z};
			for(auto P : m_Players)
			{
				Vec3f PPos = P->GetPosition();
				Vec2f PPos2D = {PPos.x, PPos.z};
				if((LEN2(PPos2D - IPos2D) < 1) &&
				   (fabs(PPos.y - IPos.y) < 5))
				{
					if(I->GetType() == ItemType::Ammo)
					{
						if(P->GetAmmo() < 100)
						{
							GameEvent EA = CreateAmmoEvent(P->GetId(), 10);
							GameEvent EI = CreateItemEvent(I->GetId());
							AddEvent(EA);
							AddEvent(EI);
						}
					}
					else if(I->GetType() == ItemType::Health)
					{
						if(P->GetHealth() < 100)
						{
							GameEvent EA = CreateHealthEvent(P->GetId(), 25);
							GameEvent EI = CreateItemEvent(I->GetId());
							AddEvent(EA);
							AddEvent(EI);
						}
					}
				}
			}
			if(I != Items.end())
				++I;
		}
	}
}

void Game::DrawUI(Shader &Shader)
{
	if(!m_Vertices.empty())
	{
		glBindVertexArray(m_VAO);
		glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
		glBufferSubData(GL_ARRAY_BUFFER, 0, m_Vertices.size() * sizeof(Vec3f), m_Vertices.data());

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_EBO);
		glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, m_Indices.size() * sizeof(uint32_t), m_Indices.data());

		glDrawElements(GL_LINES, m_Indices.size(), GL_UNSIGNED_INT, (void*)0);
	}
}

void Game::HighLight(Vec3f Origin, Vec3f Dir, float MaxRange)
{
	m_Vertices.clear();
	m_Indices.clear();
	Player->ClearText();
	float t = MaxRange;
	Vec2V3 AABB = m_Level.HighLight(Origin, Dir, t);
	Vec3f &V1 = AABB.V1;
	Vec3f &V2 = AABB.V2;
	if(LEN2(V1) + LEN2(V2))	//Hit
	{
		Vec3f Points[8] =
		{
			{V1.x, V1.y, V1.z}, {V2.x, V1.y, V1.z}, {V2.x, V1.y, V2.z}, {V1.x, V1.y, V2.z},
			{V1.x, V2.y, V1.z}, {V2.x, V2.y, V1.z}, {V2.x, V2.y, V2.z}, {V1.x, V2.y, V2.z}
		};
		for(int i = 0; i < 8; ++i)
		{
			m_Vertices.push_back(Points[i]);
			m_Vertices.push_back({1,0,1});
		}
		m_Indices.push_back(0); m_Indices.push_back(1);
		m_Indices.push_back(1); m_Indices.push_back(2);
		m_Indices.push_back(2); m_Indices.push_back(3);
		m_Indices.push_back(3); m_Indices.push_back(0);

		m_Indices.push_back(4); m_Indices.push_back(5);
		m_Indices.push_back(5); m_Indices.push_back(6);
		m_Indices.push_back(6); m_Indices.push_back(7);
		m_Indices.push_back(7); m_Indices.push_back(4);

		m_Indices.push_back(0); m_Indices.push_back(4);
		m_Indices.push_back(1); m_Indices.push_back(5);
		m_Indices.push_back(2); m_Indices.push_back(6);
		m_Indices.push_back(3); m_Indices.push_back(7);


		Player->AddText("Exit", (V1+V2)/2, {1,0,1});
	}
}

void Game::AddEvent(GameEvent Event)
{
	m_Events.push_back(Event);
}

void Game::ProcessEvent(GameEvent Event)
{
	if(Event.Type == EventType::Attack)
	{
		WeaponType Type;
		Vec3f Pos, Dir;
		std::memcpy(&Type, &Event.Data[0], sizeof(WeaponType));
		std::memcpy(&Pos, &Event.Data[1], sizeof(Vec3f));
		std::memcpy(&Dir, &Event.Data[4], sizeof(Vec3f));
		if(Type == WeaponType::Shotgun)
		{
			for(int i = 0; i < 15; ++i)
			{
				Vec2f Spread = RandGen.GetV2f(-0.1f,0.1f);
				Vec3f Right = NORMALIZE(CROSS(Dir, {0,1,0}));
				Vec3f Up = NORMALIZE(CROSS(Right, Dir));
				Vec3f DirR = NORMALIZE(Dir + Spread.x*Right + Spread.y*Up);
				Attack(Pos+0.01f*DirR, DirR, 5, AttackType::Bullet, 300.0f);
			}
		}
		else if(Type == WeaponType::Pitchfork)
		{
			Attack(Pos+0.01f*Dir, Dir, 20, AttackType::Melee, 2.0f);
		}
		else if(Type == WeaponType::Machete)
		{
			Attack(Pos+0.01f*Dir, Dir, 20, AttackType::Melee, 2.0f);
		}
		else
		{
			std::cout << "Unknown weapon type\n";
		}
	}
	else if(Event.Type == EventType::CharacterState)
	{
		uint32_t Id;
		CharacterState State;
		std::memcpy(&Id, &Event.Data[0], sizeof(uint32_t));
		std::memcpy(&State, &Event.Data[1], sizeof(CharacterState));

		SetState(Id, State);
	}
	else if(Event.Type == EventType::PlayerHealth)
	{
		uint32_t Id;
		float Health;
		std::memcpy(&Id, &Event.Data[0], sizeof(Id));
		std::memcpy(&Health, &Event.Data[1], sizeof(Health));

		ChangePlayerHealth(Id, Health);
	}
	else if(Event.Type == EventType::PlayerAmmo)
	{
		uint32_t Id;
		int32_t Ammo;
		std::memcpy(&Id, &Event.Data[0], sizeof(Id));
		std::memcpy(&Ammo, &Event.Data[1], sizeof(Ammo));

		ChangePlayerAmmo(Id, Ammo);
	}
	else if(Event.Type == EventType::ItemPickup)
	{
		uint32_t Id;
		std::memcpy(&Id, &Event.Data[0], sizeof(Id));
		RemoveItem(Id);
	}
}

void Game::SetState(uint32_t Id, CharacterState State)
{
	m_Level.SetState(Id, State);
}

void Game::ChangePlayerHealth(uint32_t Id, float Amount)
{
	for(auto &P : m_Players)
	{
		if(P->GetId() == Id)
		{
			P->ChangeHealth(Amount);
			break;
		}
	}
}

void Game::ChangePlayerAmmo(uint32_t Id, int32_t Amount)
{
	for(auto &P : m_Players)
	{
		if(P->GetId() == Id)
		{
			P->ChangeAmmo(Amount);
			break;
		}
	}
}

void Game::RemoveItem(uint32_t Id)
{
	std::vector<Item> &Items =  m_Level.GetItems();
	for(auto I = Items.begin(); I != Items.end(); ++I)
	{
		if(I->GetId() == Id)
		{
			Items.erase(I);
			break;
		}
	}
}
