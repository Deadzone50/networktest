#pragma once

#include <string>
#include <vector>
#include "Vertex.h"


void ParseObjFile(const std::string &Path, const std::string &Name, std::vector<Mesh> &Meshes);