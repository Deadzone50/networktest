#pragma once
#include <GL/glew.h>
#include <GL/glut.h>

#include <vector>
#include "Vector/Matrix.h"
#include "Vector/Vector.h"
#include "Character.h"
#include "Shader.h"

enum EnemyType
{
	Cultist, Mage
};

class EnemyClass : public Character
{
public:
	EnemyClass(EnemyType Type, Vec3f Pos);

	void SetState(CharacterState State);

	void Attack();
	void Damage(float Damage);

	void Step(float dt);
private:

	EnemyType m_Type;
	Vec3f m_Goal = {10,0,0};
	float m_Speed = 3.0f;

	uint32_t VBO;

	float m_AnimTime = 0;
	float m_DeathTime = 0;
	float m_WanderTime = 0;

	float m_DamageTaken = 0;
};
