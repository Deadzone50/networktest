#include "Network.h"

#include <iostream>
#include <Windows.h>
#include <Game.h>
#include <Player.h>

bool InitializeSockets()
{
	WSADATA WsaData;
	return WSAStartup(MAKEWORD(2,2), &WsaData) == NO_ERROR;
}

void TerminateSockets()
{
	WSACleanup();
}

Address::Address(unsigned char a, unsigned char b, unsigned char c, unsigned char d, unsigned short port)
{
	m_address = (a << 24) | (b << 16) | (c << 8) | d; 
	m_port = port;
}

Address::Address(unsigned int address, unsigned short port)
{
	m_address = address;
	m_port = port;
}

unsigned char Address::GetA() const
{
	return (m_address & 0xff000000) >> 24;
}

unsigned char Address::GetB() const
{
	return (m_address & 0x00ff0000) >> 16;
}

unsigned char Address::GetC() const
{
	return (m_address & 0x0000ff00) >> 8;
}

unsigned char Address::GetD() const
{
	return (m_address & 0x000000ff);
}

unsigned int Address::GetAddress() const
{
	return m_address;
}

unsigned short Address::GetPort() const
{
	return m_port;
}

Socket::Socket()
{
	m_handle = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if(m_handle <= 0)
		std::cout << "Error creating socket\n";
	else
		std::cout << "Socket created\n";

	DWORD nonblocking = 1;
	if(ioctlsocket(m_handle, FIONBIO, &nonblocking) != 0)
		std::cout << "Error setting non-blocking\n";
	else
		std::cout << "non-blocking set\n";
}

Socket::~Socket()
{
	Close();
}

bool Socket::Open(unsigned short port)	//for sending
{
	sockaddr_in address;
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = INADDR_ANY;
	address.sin_port = htons(port);
	return(bind(m_handle, (const sockaddr*)&address, sizeof(sockaddr_in)) >= 0);
}

void Socket::Close()
{
	closesocket(m_handle);
}

bool Socket::Send(const Address &destination, const void *data, int size)
{
	sockaddr_in address;
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = htonl(destination.GetAddress());
	address.sin_port = htons(destination.GetPort());
	int sent_bytes = sendto(m_handle, (const char*)data, size, 0, (sockaddr*)&address, sizeof(sockaddr_in));
	return (sent_bytes == size);
}

int Socket::Receive(Address &source, void *data, int size)
{
	sockaddr_in from;
	int from_len = sizeof(from);

	int bytes = recvfrom(m_handle, (char*)data, size, 0, (sockaddr*)&from, &from_len);
	if(bytes > 0)
	{
		unsigned int from_address = ntohl(from.sin_addr.s_addr);
		unsigned int from_port = ntohs(from.sin_port);
		source = Address(from_address, from_port);
	}
	return bytes;
}

bool EstablishConnection(Socket *Sock, Address Addr)
{
	bool Result = false;
	uint32_t Tries = 0;
	while(true)
	{
		++Tries;
		if(Tries > 10)
			break;
		std::cout << "Sending msg\n";
		if(!Sock->Send(Addr, "A", 1))
		{
			std::cout << "Failed to send\n";
			break;
		}
		Sleep(500);
		Address RecvAddr;
		char c;
		if(Sock->Receive(RecvAddr, &c, 1))
		{
			std::cout << "Got msg from " << (int)RecvAddr.GetA() << ":" << (int)RecvAddr.GetB() << ":" << (int)RecvAddr.GetC()
				<< ":" << (int)RecvAddr.GetD() << ":" << RecvAddr.GetPort() << " :" << c << "" << "\n";
			if(RecvAddr.GetAddress() == Addr.GetAddress())	//check that it is the correct responder
			{
				std::cout << "Sending response: ";
				std::cout << "B\n";
				if(!Sock->Send(Addr, "B", 1))
				{
					std::cout << "Failed to send\n";
					break;
				}
				if(c == 'B')
				{
					Result = true;
					break;
				}
			}
		}
	}

	return Result;
}

inline void CopyToBuffer(uint8_t *Buffer, const void *Src, size_t Size, uint32_t &Offset)
{
	std::memcpy(Buffer + Offset, Src, Size);
	Offset += Size;
}

inline void CopyFromBuffer(uint8_t *Buffer, void *Dest, size_t Size, uint32_t &Offset)
{
	std::memcpy(Dest, Buffer + Offset, Size);
	Offset += Size;
}

void CharacterDataToBuffer(uint8_t *Buffer, uint32_t &offset, const Character *Char)
{
	Vec3f Pos = Char->GetPosition();
	Vec2f Face = Char->GetFacingDirection();
	CopyToBuffer(Buffer, &Pos, sizeof(Pos), offset);
	CopyToBuffer(Buffer, &Face, sizeof(Face), offset);
}

void CharacterDataFromBuffer(uint8_t *Buffer, uint32_t &offset, Character *Char)
{
	Vec3f Pos;
	Vec2f Face;
	CopyFromBuffer(Buffer, &Pos, sizeof(Pos), offset);
	CopyFromBuffer(Buffer, &Face, sizeof(Face), offset);
	Char->SetPosition(Pos);
	Char->SetFacingDirection(Face);
}

void EventToBuffer(uint8_t *Buffer, uint32_t &offset, const GameEvent &Event)
{
	CopyToBuffer(Buffer, &Event.Type, sizeof(Event.Type), offset);
	if(Event.Type == EventType::Attack)
	{
		CopyToBuffer(Buffer, &Event.Data[0], sizeof(uint32_t), offset);		//Weapon
		CopyToBuffer(Buffer, &Event.Data[1], sizeof(Vec3f), offset);		//Pos
		CopyToBuffer(Buffer, &Event.Data[4], sizeof(Vec3f), offset);		//Dir
	}
	else if(Event.Type == EventType::CharacterState)
	{
		CopyToBuffer(Buffer, &Event.Data[0], sizeof(uint32_t), offset);		//Character Id
		CopyToBuffer(Buffer, &Event.Data[1], sizeof(uint32_t), offset);		//State
	}
	else if(Event.Type == EventType::PlayerHealth)
	{
		CopyToBuffer(Buffer, &Event.Data[0], sizeof(uint32_t), offset);		//Player Id
		CopyToBuffer(Buffer, &Event.Data[1], sizeof(float), offset);		//Health
	}
	else if(Event.Type == EventType::PlayerAmmo)
	{
		CopyToBuffer(Buffer, &Event.Data[0], sizeof(uint32_t), offset);		//Player Id
		CopyToBuffer(Buffer, &Event.Data[1], sizeof(int32_t), offset);		//Ammo
	}
	else if(Event.Type == EventType::ItemPickup)
	{
		CopyToBuffer(Buffer, &Event.Data[0], sizeof(uint32_t), offset);		//Item Id
	}
	else
	{
		std::cout << "Not implemented\n";
		abort();
	}
}

void EventFromBuffer(uint8_t *Buffer, uint32_t &offset, GameEvent &Event)
{
	CopyFromBuffer(Buffer, &Event.Type, sizeof(Event.Type), offset);
	if(Event.Type == EventType::Attack)
	{
		Event.Data.resize((sizeof(uint32_t) + 2*sizeof(Vec3f))/sizeof(uint32_t));
		CopyFromBuffer(Buffer, &Event.Data[0], sizeof(uint32_t), offset);		//Weapon
		CopyFromBuffer(Buffer, &Event.Data[1], sizeof(Vec3f), offset);			//Pos
		CopyFromBuffer(Buffer, &Event.Data[4], sizeof(Vec3f), offset);			//Dir
	}
	else if(Event.Type == EventType::CharacterState)
	{
		Event.Data.resize(2);

		CopyFromBuffer(Buffer, &Event.Data[0], sizeof(uint32_t), offset);		//Character Id
		CopyFromBuffer(Buffer, &Event.Data[1], sizeof(uint32_t), offset);		//State
	}
	else if(Event.Type == EventType::PlayerHealth)
	{
		Event.Data.resize(2);

		CopyFromBuffer(Buffer, &Event.Data[0], sizeof(uint32_t), offset);		//Player Id
		CopyFromBuffer(Buffer, &Event.Data[1], sizeof(float), offset);			//Health
	}
	else if(Event.Type == EventType::PlayerAmmo)
	{
		Event.Data.resize(2);

		CopyFromBuffer(Buffer, &Event.Data[0], sizeof(uint32_t), offset);		//Player Id
		CopyFromBuffer(Buffer, &Event.Data[1], sizeof(int32_t), offset);		//Ammo
	}
	else if(Event.Type == EventType::ItemPickup)
	{
		Event.Data.resize(1);

		CopyFromBuffer(Buffer, &Event.Data[0], sizeof(uint32_t), offset);		//Item Id
	}
	else
	{
		std::cout << "Not implemented\n";
		abort();
	}
}

GameEvent CreateAttackEvent(WeaponType Type, Vec3f Pos, Vec3f Dir)
{
	GameEvent GE;
	GE.Type = EventType::Attack;
	GE.Data.resize(1 + 3 + 3);

	std::memcpy(&GE.Data[0], &Type, sizeof(Type));
	std::memcpy(&GE.Data[1], &Pos, sizeof(Pos));
	std::memcpy(&GE.Data[4], &Dir, sizeof(Dir));

	return GE;
}

GameEvent CreateHealthEvent(uint32_t Id, float Health)
{
	GameEvent GE;
	GE.Type = EventType::PlayerHealth;
	GE.Data.resize(2);

	std::memcpy(&GE.Data[0], &Id, sizeof(Id));
	std::memcpy(&GE.Data[1], &Health, sizeof(Health));

	return GE;
}

GameEvent CreateAmmoEvent(uint32_t Id, int32_t Ammo)
{
	GameEvent GE;
	GE.Type = EventType::PlayerAmmo;
	GE.Data.resize(2);

	std::memcpy(&GE.Data[0], &Id, sizeof(Id));
	std::memcpy(&GE.Data[1], &Ammo, sizeof(Ammo));

	return GE;
}

GameEvent CreateCharacterStateEvent(uint32_t Id, CharacterState State)
{
	GameEvent GE;
	GE.Type = EventType::CharacterState;
	GE.Data.resize(2);
	std::memcpy(&GE.Data[0], &Id, sizeof(Id));
	std::memcpy(&GE.Data[1], &State, sizeof(State));

	return GE;
}

GameEvent CreateItemEvent(uint32_t ItemId)
{
	GameEvent GE;
	GE.Type = EventType::ItemPickup;
	GE.Data.resize(1);
	std::memcpy(&GE.Data[0], &ItemId, sizeof(ItemId));

	return GE;
}

extern Game App;
extern PlayerClass *Player;
Address SendAddr;

void LogBuffer(uint32_t *Buffer, uint32_t NumPlayers, uint32_t NumEnemies)
{
	//std::cout << "First char: " << Buffer[0] << "\n";
	uint32_t BufferOffset = 1;
	if(Buffer[0] == 'D')
	{
		uint32_t NumReceivedEvents;
		std::memcpy(&NumReceivedEvents, &Buffer[BufferOffset], sizeof(NumReceivedEvents));
		if(NumReceivedEvents)
		std::cout << "Events: " << NumReceivedEvents << "\n";
		++BufferOffset;
		for(uint32_t i = 0; i < NumReceivedEvents; ++i)
		{
			GameEvent Event;
			std::memcpy(&Event.Type, &Buffer[BufferOffset], sizeof(Event.Type));
			BufferOffset += 1;
			if(Event.Type == EventType::Attack)
			{
				Vec3f Pos, Dir;
				//std::memcpy(&Event.Data[0], &Buffer[BufferOffset+0], sizeof(uint32_t));		//Weapon
				std::memcpy(&Pos, &Buffer[BufferOffset+1], sizeof(Vec3f));		//Pos
				std::memcpy(&Dir, &Buffer[BufferOffset+4], sizeof(Vec3f));		//Dir
				BufferOffset += 1 + 3 + 3;

				std::cout << "Type: Attack, Pos: " << Pos <<", Dir: " << Dir << "\n";

			}
			else if(Event.Type == EventType::CharacterState)
			{
				uint32_t Id, State;
				std::memcpy(&Id, &Buffer[BufferOffset+0], sizeof(uint32_t));	//Id
				std::memcpy(&State, &Buffer[BufferOffset+1], sizeof(uint32_t));	//State
				BufferOffset += 1 + 1;

				std::cout << "Type: State, Id: " << Id <<", State: " << State << "\n";
			}
			else if(Event.Type == EventType::PlayerHealth)
			{
				uint32_t Id, Damage;
				std::memcpy(&Id, &Buffer[BufferOffset+0], sizeof(uint32_t));	//Id
				std::memcpy(&Damage, &Buffer[BufferOffset+1], sizeof(float));	//Amount
				BufferOffset += 1 + 1;

				std::cout << "Type: Damage, Id: " << Id <<", Damage: " << Damage << "\n";
			}
		}

		//for(uint32_t i = 0; i < NumPlayers; ++i)
		//{
		//	Vec3f Pos;
		//	Vec2f Face;
		//	std::memcpy(&Pos, &Buffer[BufferOffset], sizeof(Pos));
		//	std::memcpy(&Face, &Buffer[BufferOffset+3], sizeof(Face));
		//	BufferOffset += 3 + 2;

		//	std::cout << "Player["<<i<<"], Pos: "<< Pos << ", Face: "<< Face << "\n";
		//}

		//for(uint32_t i = 0; i < NumEnemies; ++i)
		//{
		//	Vec3f Pos;
		//	Vec2f Face;
		//	std::memcpy(&Pos, &Buffer[BufferOffset], sizeof(Pos));
		//	std::memcpy(&Face, &Buffer[BufferOffset+3], sizeof(Face));
		//	BufferOffset += 3 + 2;

		//	std::cout << "Enemy["<<i<<"], Pos: "<< Pos << ", Face: "<< Face << "\n";
		//}
	}

}

bool ReceiveEventsLoop(Socket *Sock, uint8_t *Buffer, uint32_t &BufferOffset)
{
	Address Addr;
	bool rec = false;
	while(true)
	{
		uint8_t Tmp[512] = {};
		int RBytes = Sock->Receive(Addr, Tmp, 512);
		if(RBytes > 0)
		{
			std::memcpy(Buffer, Tmp, 512);
			if(Buffer[0] == 'D')
			{
				rec = true;
				BufferOffset = 1;
				//Events
				uint32_t NumReceivedEvents;
				CopyFromBuffer(Buffer, &NumReceivedEvents, sizeof(NumReceivedEvents), BufferOffset);
				for(uint32_t i = 0; i < NumReceivedEvents; ++i)
				{
					GameEvent E;
					EventFromBuffer(Buffer, BufferOffset, E);
					App.AddEvent(E);
				}
				//std::cout << "REC: " << RBytes << "\n";
				//if(RBytes > 200)
				//{
				//	std::cout << "OVER 200\n";
				//}
			}
		}
		else if(RBytes < 0)
		{
			int error = WSAGetLastError();
			if(error != 10035)	//WSAEWOULDBLOCK
			{
				std::cout << "Socket error: " << error <<"\n";
			}
			break;
		}
		else
		{
			break;
		}
	}
	return rec;
}

void ClientSync(Socket *Sock)
{
	uint8_t Buffer[512] = {};
	uint32_t BufferOffset = 0;

	std::vector<PlayerClass*> &Players = App.GetPlayers();
	std::vector<EnemyClass> &Enemies = App.GetCurrentLevel().GetEnemies();
	std::vector<GameEvent> OwnEvents = App.GetEvents();
	uint32_t NumOwnEvents = OwnEvents.size();
	App.ClearEvents();

	bool rec = ReceiveEventsLoop(Sock, Buffer, BufferOffset);
	if(rec)											//RECV
	{
		//Players
		for(auto &P : Players)
		{
			if(P->GetId() != Player->GetId())		//Skip self
			{
				CharacterDataFromBuffer(Buffer, BufferOffset, P);
			}
			else
			{
				BufferOffset += sizeof(Vec3f) + sizeof(Vec2f);
			}
		}

		//Enemies
		for(auto &E : Enemies)
		{
			CharacterDataFromBuffer(Buffer, BufferOffset, &E);
		}
	}
	{												//SEND
		std::memset(Buffer, 0, 512);
		Buffer[0] = 'D';
		uint32_t BufferOffset = 1;
		//Events
		CopyToBuffer(Buffer, &NumOwnEvents, sizeof(NumOwnEvents), BufferOffset);
		for(const auto &E : OwnEvents)
		{
			EventToBuffer(Buffer, BufferOffset, E);
		}

		//Players
		uint32_t Id = Player->GetId();
		CopyToBuffer(Buffer, &Id, sizeof(Id), BufferOffset);
		CharacterDataToBuffer(Buffer, BufferOffset, Player);

		//std::cout << "SEND: " << BufferOffset << "\n";
		//if(BufferOffset > 100)
		//{
 	//		std::cout << "OVER 100\n";
		//}
		Sock->Send(SendAddr, Buffer, BufferOffset);
	}
}

void HostSync(Socket *Sock)
{
	uint8_t Buffer[512] = {};
	uint32_t BufferOffset = 0;

	std::vector<PlayerClass*> &Players = App.GetPlayers();
	std::vector<EnemyClass> &Enemies = App.GetCurrentLevel().GetEnemies();

	bool rec = ReceiveEventsLoop(Sock, Buffer, BufferOffset);
	if(rec)											//RECV
	{
		//Player
		uint32_t Id;
		CopyFromBuffer(Buffer, &Id, sizeof(Id), BufferOffset);
		for(auto &P : Players)
		{
			if(P->GetId() == Id)					//set the correct player
			{
				CharacterDataFromBuffer(Buffer, BufferOffset, P);
			}
		}
	}
	{
		std::memset(Buffer, 0, 512);				//SEND
		Buffer[0] = 'D';
		BufferOffset = 1;

		//Events
		std::vector<GameEvent> &Events = App.GetEvents();
		uint32_t NumEvents = Events.size();
		CopyToBuffer(Buffer, &NumEvents, sizeof(NumEvents), BufferOffset);
		for(const auto &E : Events)
		{
			EventToBuffer(Buffer, BufferOffset, E);
		}

		//Players
		for(const auto &P : Players)
		{
			CharacterDataToBuffer(Buffer, BufferOffset, P);
		}

		//Enemies
		for(const auto &E : Enemies)
		{
			CharacterDataToBuffer(Buffer, BufferOffset, &E);
		}

		std::cout << "SEND: " << BufferOffset << "\n";
		Sock->Send(SendAddr, Buffer, BufferOffset);
	}
}
