#include "Player.h"

#include <iostream>
#include "Collision.h"
#include "Map.h"
#include "TextureManager.h"
#include "Weapon.h"
#include "Random.h"
#include "DebugDraws.h"
#include "Game.h"
#include "Sound.h"

extern TextureManager TexManager;
extern RandomGenerator RandGen;
extern DebugDraws DebugDrawer;
extern Game App;
extern SoundManager AudioManager;

PlayerClass::PlayerClass()
{
	SetRotation({0, -3.14f/2});
	m_Pos = {0,0,0};
	m_CurrentSpeed = {0,0,0};

	m_UIProjectionMatrix = Ortho(0, 800, 0, 600, -1, 1);
	m_UITextureId = TexManager.Texture("Assets/Sprites/UI.png");
	m_TextureId = TexManager.Texture("Assets/Sprites/Player.png");
	Vertex Quad[12];
	Quad[0].TexCoord = {0,0};
	Quad[1].TexCoord = {1,0};
	Quad[2].TexCoord = {1,1};
	Quad[3].TexCoord = {0,1};

	Quad[4].TexCoord = {0,0};
	Quad[5].TexCoord = {1,0};
	Quad[6].TexCoord = {1,1};
	Quad[7].TexCoord = {0,1};

	Quad[8].TexCoord = {0,0};
	Quad[9].TexCoord = {1,0};
	Quad[10].TexCoord = {1,1};
	Quad[11].TexCoord = {0,1};

	Quad[0].Position = {240,	0,		0};			//Weapon
	Quad[1].Position = {560,	0,		0};
	Quad[2].Position = {560,	240,	0};
	Quad[3].Position = {240,	240,	0};

	Quad[4].Position = {0,		0,		-0.1f};		//Health
	Quad[5].Position = {240,	0,		-0.1f};
	Quad[6].Position = {240,	150,	-0.1f};
	Quad[7].Position = {0,		150,	-0.1f};

	Quad[8].Position = {560,	0,		-0.1f};		//Ammo
	Quad[9].Position = {800,	0,		-0.1f};
	Quad[10].Position = {800,	150,	-0.1f};
	Quad[11].Position = {560,	150,	-0.1f};

	glGenVertexArrays(1, &m_VAO);
	glGenBuffers(1, &m_VBO);
	glGenBuffers(1, &m_EBO);

	glBindVertexArray(m_VAO);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Quad) + sizeof(Vertex)*24, NULL, GL_STATIC_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, sizeof(Vertex)*24, sizeof(Quad), &Quad[0]);
	// position
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Position));
	glEnableVertexAttribArray(0);
	// texture
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, TexCoord));
	glEnableVertexAttribArray(1);
	// color
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Color));
	glEnableVertexAttribArray(2);

	uint32_t Indices[12] = { 24,25,26,27, 28,29,30,31, 32,33,34,35 };
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(Indices) + sizeof(uint32_t)*24, NULL, GL_STATIC_DRAW);
	glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint32_t)*24, sizeof(Indices), &Indices[0]);

	UpdateUI();

	glGenVertexArrays(1, &m_VAOText);
	glGenBuffers(1, &m_VBOText);
	glGenBuffers(1, &m_EBOText);

	glBindVertexArray(m_VAOText);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOText);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex)*256, NULL, GL_STREAM_DRAW);
	// position
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Position));
	glEnableVertexAttribArray(0);
	// texture
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, TexCoord));
	glEnableVertexAttribArray(1);
	// color
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Color));
	glEnableVertexAttribArray(2);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_EBOText);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint32_t)*256, NULL, GL_STREAM_DRAW);

	m_TextShader = Shader("Shader/text.vert", "Shader/text.frag");
	m_TextShader.Use();
	m_TextShader.SetSampler("Texture", 1);

	m_Shader = Shader("Shader/UI.vert", "Shader/UI.frag");
	m_Shader.Use();
	m_Shader.SetMat4("Projection", m_UIProjectionMatrix);
	m_Shader.SetSampler("Texture", 0);
}

void PlayerClass::Reset()
{
	SetRotation({0, -3.14f/2});
	m_Pos = {0,0,0};
	m_CurrentSpeed = {0,0,0};

	m_Health = 100;
	m_Ammo = 20;
	m_Falling = true;
	UpdateUI();
}

void PlayerClass::AddText(const std::string &Text, const Vec3f &Position, const Vec3f &Color)
{
	m_Text.push_back(Text);
	float Len = Text.size() * 9*0.05f;
	m_TextPos.push_back(Position - Len/2.0f*m_Right);
	m_TextCol.push_back(Color);
}

void PlayerClass::ClearText()
{
	m_Text.clear();
	m_TextPos.clear();
	m_TextCol.clear();
}

void PlayerClass::UpdateUI()
{
	std::vector<Vertex> Vertices;
	std::vector<uint32_t> Indices;

	std::string h = std::to_string((unsigned)m_Health);
	if(h.size() == 1) h+="  ";
	if(h.size() == 2) h+=" ";
	std::string a = std::to_string(m_Ammo);
	if(a.size() == 1) a+="  ";
	if(a.size() == 2) a+=" ";
		
	CreateTextMesh(h, {150,5,0}, {1,0,0}, {0,1,0}, 2, {0.6f,0,0}, Vertices, Indices);

	CreateTextMesh(a, {720,5,0}, {1,0,0}, {0,1,0}, 2, {0.6f,0.6f,0.6f}, Vertices, Indices);



	glBindVertexArray(m_VAO);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(Vertex)*Vertices.size(), Vertices.data());
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_EBO);
	glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, sizeof(uint32_t)*Indices.size(), Indices.data());
}

void PlayerClass::DrawUI(const Mat4f &ProjectionMatrix)
{
	glBindVertexArray(m_VAO);

	m_Shader.Use();
	glBindTexture(GL_TEXTURE_2D, m_UITextureId);	//bind texture to texture unit 0
	//m_Shader.SetMat4("Projection", m_ProjectionMatrix);
	//m_Shader.SetSampler("Texture", 0);

	m_Shader.SetInt("AnimFrame", 0);				//Health
	m_Shader.SetInt("Row", 2);	
	glDrawElements(GL_QUADS, 4, GL_UNSIGNED_INT, (void*)(28*sizeof(uint32_t)));

	m_Shader.SetInt("AnimFrame", 1);				//Ammo
	m_Shader.SetInt("Row", 2);	
	glDrawElements(GL_QUADS, 4, GL_UNSIGNED_INT, (void*)(32*sizeof(uint32_t)));

	m_Shader.SetInt("AnimFrame", m_AnimFrame);		//Weapon
	m_Shader.SetInt("Row", m_AnimRow);
	glDrawElements(GL_QUADS, 4, GL_UNSIGNED_INT, (void*)(24*sizeof(uint32_t)));

	const Mat4f Identity;

	m_TextShader.Use();
	m_TextShader.SetMat4("View", Identity);
	m_TextShader.SetMat4("Projection", m_UIProjectionMatrix);
	glDrawElements(GL_QUADS, 12, GL_UNSIGNED_INT, (void*)(0*sizeof(uint32_t)));		//Health number
	glDrawElements(GL_QUADS, 12, GL_UNSIGNED_INT, (void*)(12*sizeof(uint32_t)));	//Ammo number


	m_Vertices.clear();
	m_Indices.clear();
	for(int i = 0; i < m_Text.size(); ++i)
	{
		CreateTextMesh(m_Text[i], m_TextPos[i], m_Right, m_Up, 0.05f, m_TextCol[i], m_Vertices, m_Indices);
	}

	glBindVertexArray(m_VAOText);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOText);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(Vertex)*m_Vertices.size(), m_Vertices.data());
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_EBO);
	glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, sizeof(uint32_t)*m_Indices.size(), m_Indices.data());

	glBindVertexArray(m_VAOText);
	m_TextShader.SetMat4("View", GetViewMatrix());
	m_TextShader.SetMat4("Projection", ProjectionMatrix);
	glDrawElements(GL_QUADS, m_Indices.size(), GL_UNSIGNED_INT, (void*)0);			//popup text
}

void PlayerClass::Attack()
{
	if(!(m_State & CharacterState::Attacking) &&
	   !(m_State & CharacterState::Dead))
	{
		if(m_CurrentWeapon == WeaponType::Shotgun && m_Ammo > 0)
		{
			m_State = m_State | CharacterState::Attacking | CharacterState::Animating;
			AudioManager.Play("Shotgun2", m_Pos, 0.5f);
			m_Ammo--;
			UpdateUI();

			GameEvent E = CreateAttackEvent(WeaponType::Shotgun, m_Pos, m_LookingDirection);
			App.AddEvent(E);
		}
		else if(m_CurrentWeapon == WeaponType::Machete)
		{
			m_State = m_State | CharacterState::Attacking | CharacterState::Animating;
			AudioManager.Play("Swing", m_Pos);

			GameEvent E = CreateAttackEvent(WeaponType::Machete, m_Pos, m_LookingDirection);
			App.AddEvent(E);
		}
	}
}

void PlayerClass::ChangeHealth(float Amount)
{
	m_Health += Amount;
	if(Amount > 0)
	{
		AudioManager.Play("Pickup-Health", m_Pos);
	}
	if(m_Health < 0)
	{
		m_Health = 0;
	//	m_Dead = true;
	//	m_Attacking = false;
	//	m_Animating = true;
	//	m_Timer = 0;
	}
	else if(m_Health > 100)
	{
		m_Health = 100;
	}
	UpdateUI();
}

void PlayerClass::ChangeAmmo(int32_t Amount)
{
	if(Amount > 0)
	{
		AudioManager.Play("Pickup-Ammo", m_Pos);
		m_Ammo += Amount;
		if(m_Ammo > 100)
			m_Ammo = 100;
	}
	else
	{
		std::cout << "Removing ammo not implemented\n";
		abort();
	}
	UpdateUI();
}

void PlayerClass::SetRotation(Vec2f Angles)
{
	m_Rotation = Angles;

	m_LookingDirection.x = cos(m_Rotation.x) * cos(m_Rotation.y);
	m_LookingDirection.y = sin(m_Rotation.x);
	m_LookingDirection.z = cos(m_Rotation.x) * sin(m_Rotation.y);
	m_LookingDirection = NORMALIZE(m_LookingDirection);

	m_Right = NORMALIZE(CROSS(m_LookingDirection, { 0,1,0 }));
	m_Up = NORMALIZE(CROSS(m_Right, m_LookingDirection));

	m_LookingDirection2D = NORMALIZE(Vec2f{m_LookingDirection.x, m_LookingDirection.z});
}

void PlayerClass::Rotate(Vec2f Angles)
{
	m_Rotation += Angles;

	if(m_Rotation.x > 1.55f)
		m_Rotation.x = 1.55f;
	if(m_Rotation.x < -1.55f)
		m_Rotation.x = -1.55f;

	SetRotation(m_Rotation);
}

Mat4f PlayerClass::GetViewMatrix()
{
	return LookAt(m_Pos, m_Pos+m_LookingDirection, m_Up);
}

void PlayerClass::Move(const Vec3f &Amount)
{
	if(m_Debug)
	{
		m_Pos += Amount.x*m_Right + Amount.y*m_Up -Amount.z*m_LookingDirection;
	}
	else
	{
		Vec3f PlaneForward = NORMALIZE({m_LookingDirection.x, 0, m_LookingDirection.z});
		m_Pos += Amount.x*m_Right -Amount.z*PlaneForward;
	}
}

void PlayerClass::Jump()
{
	if(!m_Falling)
	{
		std::vector<Mesh*> LevelMeshes = App.GetCurrentLevel().GetMeshes();
		float tUp = FLT_MAX;
		for(int i = 0; i < LevelMeshes[0]->Indices.size(); i+=3)
		{
			const Vertex &V1 = LevelMeshes[0]->Vertices[LevelMeshes[0]->Indices[i]];
			const Vertex &V2 = LevelMeshes[0]->Vertices[LevelMeshes[0]->Indices[i+1]];
			const Vertex &V3 = LevelMeshes[0]->Vertices[LevelMeshes[0]->Indices[i+2]];
			Vec3f VP[3] = {V1.Position, V2.Position, V3.Position};
			if(fabs(DOT(V1.Normal, {0,1,0})) > 0.1f)	//if floor/ceiling
			{
				RayTriangleIntersection(m_Pos, {0,1,0}, VP, tUp);
			}
		}

		if(tUp > 0.3f)
		{
			Vec3f CurrentPos = GetPosition();
			m_CurrentSpeed.y = 10.0f;
			SetPosition(CurrentPos + Vec3f{0,0.1f,0});
			m_Falling = true;
		}
	}
}

void PlayerClass::Interact()
{
	App.Interact(m_Pos, m_LookingDirection, 2);
}

void PlayerClass::Step(float dt)
{
	if(!!(m_State & CharacterState::Dead))
	{
		m_Pos -= Vec3f{0,2.0f,0}*dt;
		if(m_Pos.y < 0.2f)
			m_Pos.y = 0.2f;
	}
	else if(!m_Debug)
	{
		Vec3f NewPos = HandleCollision(dt);
		m_PrevPos = m_Pos + NewPos;
		m_Pos = m_Pos + NewPos + m_CurrentSpeed*dt;
	}

	if(!!(m_State & CharacterState::Animating) || !!(m_State & CharacterState::Attacking))
	{
		m_Timer += dt;
		if(!!(m_State & CharacterState::Animating))
		{
			if(m_Timer > m_FrameTime*(m_AnimFrame+1))
			{
				m_AnimFrame++;
				if(m_AnimFrame > 3)
				{
					m_State = m_State & ~CharacterState::Animating;
					m_AnimFrame = 0;
				}
			}
		}
		if(!!(m_State & CharacterState::Attacking))
		{
			if(m_Timer > m_AttackTime)
			{
				m_State = m_State & ~CharacterState::Attacking;
				m_Timer = 0;
			}
		}
		else
		{		//Switching weapons

		}
	}

	App.HighLight(m_Pos, m_LookingDirection, 2);
}

void PlayerClass::SwitchWeapon(uint8_t Number)
{
	switch(Number)
	{
		case 1:
			if(m_CurrentWeapon != WeaponType::Machete)
			{
				m_CurrentWeapon = WeaponType::Machete;
				m_AnimRow = 1;
				m_AttackTime = 0.4f;
				//m_State = m_State | CharacterState::Animating;
			}
			break;
		case 2:
			if(m_CurrentWeapon != WeaponType::Shotgun)
			{
				m_CurrentWeapon = WeaponType::Shotgun;
				m_AnimRow = 3;
				m_AttackTime = 0.8f;
				//m_State = m_State | CharacterState::Animating;
			}
			break;
	}
}
