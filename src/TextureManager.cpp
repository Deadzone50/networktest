#include "TextureManager.h"
#include <iostream>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

TextureManager::TextureManager()
{
	stbi_set_flip_vertically_on_load(true);
}

uint32_t TextureManager::Texture(const std::string &FileName)
{
	uint32_t TexId = m_Textures[FileName];
	if(!TexId)
	{
		int W, H, BPP;
		unsigned char *Data = stbi_load(FileName.c_str(), &W, &H, &BPP, 0);

		glGenTextures(1, &TexId);
		glBindTexture(GL_TEXTURE_2D, TexId);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		if(BPP == 3)																			//RGB
			glTexImage2D(GL_TEXTURE_2D,	0, GL_SRGB, W, H, 0, GL_RGB, GL_UNSIGNED_BYTE, Data);
		else if(BPP ==4)																		//RGBA
			glTexImage2D(GL_TEXTURE_2D,	0, GL_SRGB_ALPHA, W, H, 0, GL_RGBA, GL_UNSIGNED_BYTE, Data);//SRGB gives a reverse gamma correction
		else
			std::cout << "unknown imageformat\n";
		//target, level, 0 = base no minimap, interalformat, width, height, border, format, type
		//glBindTexture(GL_TEXTURE_2D, 0);

		stbi_image_free(Data);
		m_Textures[FileName] = TexId;
	}
	return TexId;
}
