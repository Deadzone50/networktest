#pragma once

#include <vector>
#include "Vector/Vector.h"

struct Vertex
{
	Vec3f Position = {};
	Vec3f Color = {0.5, 0.5, 0.5};
	Vec3f Normal = {};
	Vec2f TexCoord = {};
};

bool operator==(const Vertex& lhs, const Vertex& rhs);

class Mesh
{
public:
	bool RayCollision(Vec3f Origin, Vec3f Direction, float &Max);

	void SetPos(const Vec3f &pos);
	void Move(const Vec3f &amount);
	void Rotate(const Vec3f &angles);
	void Append(const Mesh &mesh);

	std::vector<uint32_t> Indices;
	std::vector<Vertex> Vertices;
	Vec3f m_Pos = {0,0,0};
};

//Creates Quads with letters
void CreateTextMesh(std::string Text, Vec3f Position, Vec3f Right, Vec3f Up, float Scale, Vec3f Color, std::vector<Vertex> &Vertices, std::vector<uint32_t> &Indices);
