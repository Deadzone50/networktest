#include "Character.h"

#include <vector>
#include "Vertex.h"
#include "Game.h"
#include "Collision.h"

bool operator !(const CharacterState &rhs)
{
	return rhs == CharacterState::None;
}

CharacterState operator &(CharacterState lhs, CharacterState rhs)
{
	return static_cast<CharacterState>(
		static_cast<std::underlying_type<CharacterState>::type>(lhs) &
		static_cast<std::underlying_type<CharacterState>::type>(rhs));
}

CharacterState operator |(CharacterState lhs, CharacterState rhs)
{
	return static_cast<CharacterState>(
		static_cast<std::underlying_type<CharacterState>::type>(lhs) |
		static_cast<std::underlying_type<CharacterState>::type>(rhs));
}

CharacterState operator ~(CharacterState rhs)
{
	return static_cast<CharacterState>(~static_cast<std::underlying_type<CharacterState>::type>(rhs));
}

extern Game App;

Character::Character()
{
	static uint32_t id = 0;
	m_Id = id++;
}

void Character::SetFacingDirection(Vec2f FacingDirection)
{
	m_LookingDirection2D = FacingDirection;
	m_Right2D = Rotate90CCW(m_LookingDirection2D);
}

Vec3f Character::HandleCollision(float dt)
{
	const float dist = 1.0f;

	// Wall collision
	std::vector<Mesh*> LevelMeshes = App.GetCurrentLevel().GetMeshes();
	float t1 = dist*2;
	float t2 = dist*2;
	float t3 = dist*2;
	float t4 = dist*2;
	float tDown = dist*2;
	float tUp = dist*2;
#if 1
	for(int i = 0; i < LevelMeshes[0]->Indices.size(); i+=3)
	{
		const Vertex &V1 = LevelMeshes[0]->Vertices[LevelMeshes[0]->Indices[i]];
		const Vertex &V2 = LevelMeshes[0]->Vertices[LevelMeshes[0]->Indices[i+1]];
		const Vertex &V3 = LevelMeshes[0]->Vertices[LevelMeshes[0]->Indices[i+2]];
		Vec3f VP[3] = {V1.Position, V2.Position, V3.Position};
		if(fabs(DOT(V1.Normal, {0,1,0})) > 0.7f)	//if ceiling/floor
		{
			RayTriangleIntersection(m_Pos, {0,-1,0}, VP, tDown);
			RayTriangleIntersection(m_Pos, {0,1,0}, VP, tUp);
		}
		else
		{
			RayTriangleIntersection(m_Pos, {-1,0,0}, VP, t1);
			RayTriangleIntersection(m_Pos, {0,0,-1}, VP, t2);
			RayTriangleIntersection(m_Pos, {1,0,0}, VP, t3);
			RayTriangleIntersection(m_Pos, {0,0,1}, VP, t4);
		}
	}
#else
	LevelMeshes[0]->RayCollision(m_Pos, {-1,0,0}, t1);
	LevelMeshes[0]->RayCollision(m_Pos, {0,0,-1}, t2);
	LevelMeshes[0]->RayCollision(m_Pos, {1,0,0}, t3);
	LevelMeshes[0]->RayCollision(m_Pos, {0,0,1}, t4);

	LevelMeshes[0]->RayCollision(m_Pos, {0,-1,0}, tDown);
	LevelMeshes[0]->RayCollision(m_Pos, {0,1,0}, tUp);
#endif
	if(tDown < 1.7f -0.01f && tUp < 1.7f - tDown)	//too tight space
	{
		m_Pos = m_PrevPos;			//prevent move				//TODO: test
		//Vec3f tmp = (m_PrevPos - m_Pos) * 30.0f;
		//m_Goal = {tmp.x, tmp.z};// + RandGen.GetRandomV3f(-10, 10);
		return {0,0,0};
	}

	// Enemy collision
	std::vector<EnemyClass> Enemies = App.GetCurrentLevel().GetEnemies();
	for(int i = 0; i < Enemies.size(); ++i)
	{
		if(Enemies[i].GetId() != m_Id)
		{
			Enemies[i].RayCollision(m_Pos, {-1,0,0}, t1);
			Enemies[i].RayCollision(m_Pos, {0,0,-1}, t2);
			Enemies[i].RayCollision(m_Pos, {1,0,0}, t3);
			Enemies[i].RayCollision(m_Pos, {0,0,1}, t4);
		}
	}

	Vec3f NewPos = {0,0,0};
	if(t1- dist< 0)
	{
		NewPos += (t1-dist)*Vec3f{-1,0,0};
	}
	if(t2-dist < 0)
	{
		NewPos += (t2-dist)*Vec3f{0,0,-1};
	}
	if(t3-dist < 0)
	{
		NewPos += (t3-dist)*Vec3f{1,0,0};
	}
	if(t4-dist < 0)
	{
		NewPos += (t4-dist)*Vec3f{0,0,1};
	}

	//Gravity
	if(tUp < 0.1f)					//touching roof
	{
		m_CurrentSpeed.y = 0;
	}
	if(tDown > 1.7f +0.01f)			//in air
	{
		m_CurrentSpeed.y -= 9.81f*dt * 5;
		m_Falling = true;
	}
	else if(tDown < 1.7f -0.01f)	//under floor
	{
		NewPos.y += 1.7f - tDown;
		m_CurrentSpeed.y = 0;
		m_Falling = false;
	}
	return NewPos;
}

void Character::Draw(Shader &Shader, Vec3f CameraPos) const
{
	int RotationIndex = 0;
	if(!(m_State & CharacterState::Dead))
	{
		Vec3f CameraDir = NORMALIZE(CameraPos - m_Pos);
		float Front = DOT(m_LookingDirection2D, Vec2f{CameraDir.x, CameraDir.z});
		float Side = DOT(m_Right2D, Vec2f{CameraDir.x, CameraDir.z});

		if(fabs(Front) > fabs(Side))	//more from front/back
		{
			if(Front > 0)
			{
				RotationIndex = 0;
			}
			else
			{
				RotationIndex = 1;
			}
		}
		else						//more from side
		{
			if(Side > 0)
			{
				RotationIndex = 3;
			}
			else
			{
				RotationIndex = 2;
			}
		}
	}
	else
	{
		RotationIndex = 4;
	}

	glBindTexture(GL_TEXTURE_2D, m_TextureId);	//bind texture to texture unit 0
	Shader.SetVec3("Pos", m_Pos-Vec3f{0,0.7f,0});
	//Shader.SetVec3("CameraPos", CameraPos);

	Shader.SetInt("AnimFrame", m_CurrentFrame);
	Shader.SetInt("RotationIndex", RotationIndex);
	glDrawElements(GL_QUADS, 4, GL_UNSIGNED_INT, (void*)0);
}

bool Character::RayCollision(Vec3f Origin, Vec3f Direction, float &Max)
{
	if(m_State != CharacterState::Dead)
	{
		float t = Max;
		Vec3f tmp = -Direction;
		tmp.y = 0;
		Vec3f ToOrigin = NORMALIZE(tmp);
		Vec3f Corner0, Corner1, Corner2, Corner3;
		Vec3f ToRight = 0.4f*CROSS(ToOrigin, {0,-1,0});
		Vec3f PlaneNormal;
		if(fabs(DOT(Direction, {0,1,0})) < 0.99f)
		{
			Vec3f Up = {0,1,0};
			Corner0 = m_Pos - ToRight - 1.7f*Up;
			Corner1 = m_Pos + ToRight - 1.7f*Up;
			Corner2 = m_Pos + ToRight + 0.3f*Up;
			Corner3 = m_Pos - ToRight + 0.3f*Up;
			PlaneNormal = ToOrigin;
		}
		else
		{
			Vec3f Back = 0.1f*-ToOrigin;
			Corner0 = m_Pos - ToRight - Back;
			Corner1 = m_Pos + ToRight - Back;
			Corner2 = m_Pos + ToRight + Back;
			Corner3 = m_Pos - ToRight + Back;
			PlaneNormal = {0,1,0};
		}
		if(RayPlaneCollisionTest(PlaneNormal, m_Pos, Origin, Direction, t))
		{
			//DebugDrawer.DrawLine(Corner0, Corner1, {0,1,0});
			//DebugDrawer.DrawLine(Corner1, Corner2, {0,1,0});
			//DebugDrawer.DrawLine(Corner2, Corner3, {0,1,0});
			//DebugDrawer.DrawLine(Corner3, Corner0, {0,1,0});

			Vec3f V01 = NORMALIZE(Corner1-Corner0);
			Vec3f V03 = NORMALIZE(Corner3-Corner0);
			Vec3f V21 = NORMALIZE(Corner1-Corner2);
			Vec3f V23 = NORMALIZE(Corner3-Corner2);
			Vec3f ColPoint = Origin + t*Direction;

			Vec3f V0P = NORMALIZE(ColPoint-Corner0);
			Vec3f V2P = NORMALIZE(ColPoint-Corner2);
			if(	DOT(V01, V0P) > 0 && DOT(V03, V0P) > 0 &&
			   DOT(V21, V2P) > 0 && DOT(V23, V2P) > 0)
			{
				Max = t;
				return true;
			}
		}
	}
	return false;
}
