#pragma once
#include <GL/glew.h>
#include <GL/glut.h>

#include <vector>
#include "Vector/Vector.h"
#include "Shader.h"

struct Particle
{
	Vec3f Position, Color, CurrentSpeed;
	float TimeLeft;
};

class ParticleSystem
{
public:
	void Initialize();
	void AddParticle(Vec3f Pos, Vec3f Col, Vec3f Speed, float Time);
	void UpdateBuffers();

	void Step(float dt);
	void Draw(const Mat4f &View, const Mat4f &Projection);
private:
	std::vector<Particle> m_Particles;
	uint32_t m_NumParticles;
	uint32_t m_VAO, m_VBO, m_EBO;
	Shader m_Shader;
};
