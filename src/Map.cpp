#include "Map.h"
#include "FileIO.h"
#include "Player.h"
#include "Collision.h"
#include "DebugDraws.h"
#include "Game.h"
#include "Random.h"
#include "TextureManager.h"

extern Game App;
extern DebugDraws DebugDrawer;
extern RandomGenerator RandGen;
extern TextureManager TexManager;

Item::Item(ItemType Type, Vec3f Pos)
{
	static uint32_t id = 0;
	m_Id = id++;

	m_Pos = Pos;
	m_Type = Type;

	m_TextureId = TexManager.Texture("Assets/Sprites/Items.png");
	switch(Type)
	{
		case ItemType::Health:
			m_TextureRow = 0;
			m_TextureCol = 0;
			break;
		case ItemType::Ammo:
			m_TextureRow = 0;
			m_TextureCol = 1;
			break;
	}
}

void Item::Draw(Shader & Shader)
{
	glBindTexture(GL_TEXTURE_2D, m_TextureId);	//bind texture to texture unit 0
	Shader.SetVec3("Pos", m_Pos);

	Shader.SetFloat("TextureScale", 0.25f);
	Shader.SetInt("AnimFrame", m_TextureCol);
	Shader.SetInt("RotationIndex", m_TextureRow);
	glDrawElements(GL_QUADS, 4, GL_UNSIGNED_INT, (void*)0);
}

static void AlignMesh(Mesh &mesh)
{
	Vec3f Min = {FLT_MAX, FLT_MAX, FLT_MAX};
	Vec3f Max = {FLT_MIN, FLT_MIN, FLT_MIN};
	for(auto &V : mesh.Vertices)
	{
		if(V.Position.x < Min.x)
			Min.x = V.Position.x;
		if(V.Position.y < Min.y)
			Min.y = V.Position.y;
		if(V.Position.z < Min.z)
			Min.z = V.Position.z;

		if(V.Position.x > Max.x)
			Max.x = V.Position.x;
		if(V.Position.y > Max.y)
			Max.y = V.Position.y;
		if(V.Position.z > Max.z)
			Max.z = V.Position.z;
	}
	Vec3f Center = (Min + Max) /2;

	for(auto &V : mesh.Vertices)
	{
		V.Position.x -= Center.x;
		V.Position.z -= Center.z;

		V.Position.y -= Min.y;
	}
}

void Map::Initialize()
{
	glGenVertexArrays(1, &m_VAO);
	glGenBuffers(1, &m_VBO);
	glGenBuffers(1, &m_EBO);

	//m_Exit.Initialize();

	ParseObjFile("Assets/Obj/", "Stairs.obj", m_LayoutMeshes);
	ParseObjFile("Assets/Obj/", "dungeon.obj", m_LayoutMeshes);
	ParseObjFile("Assets/Obj/", "dungeon2.obj", m_LayoutMeshes);

	AlignMesh(m_LayoutMeshes[0]);
	AlignMesh(m_LayoutMeshes[1]);
	AlignMesh(m_LayoutMeshes[2]);

	m_Exit.SetFunction([&]{LoadNextLevel();});
}

void Map::Reset()
{
	GenerateLevel();
}

void Map::LoadNextLevel()
{
	Reset();
	App.SwitchState(GameState::DisplayMenu);
	App.Stop();
}

void Map::GenerateLevel()
{
	m_Enemies.clear();

	for(auto ptr : m_Meshes)
	{
		delete ptr;
	}
	m_Meshes.clear();

	Mesh *LevelGeometry = new Mesh;
	m_Meshes.push_back(LevelGeometry);

	//m_Entrance.SetPosition({100,0,-35});
	m_Entrance.SetMesh(&m_LayoutMeshes[0]);

	Mesh Dungeon0 = m_LayoutMeshes[1];
	Mesh Dungeon1 = m_LayoutMeshes[2];
	//LevelGeometry->Append(Dungeon0);
	LevelGeometry->Append(Dungeon1);

	Mesh Stairs = m_LayoutMeshes[0];
	//Stairs.Rotate({0,3.14f,0});
	//Stairs.Move({-28,0,17});
	Stairs.Move({55,20,-67});
	LevelGeometry->Append(Stairs);

	//m_Exit.SetPosition({-28,0,17});
	m_Exit.SetMesh(&Stairs);

	glBindVertexArray(m_VAO);

	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex)*m_Meshes[0]->Vertices.size(), m_Meshes[0]->Vertices.data(), GL_STATIC_DRAW);

	// position
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Position));
	glEnableVertexAttribArray(0);
	// color
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Color));
	glEnableVertexAttribArray(1);
	// normal
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Normal));
	glEnableVertexAttribArray(2);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint32_t)*m_Meshes[0]->Indices.size(), m_Meshes[0]->Indices.data(), GL_STATIC_DRAW);


	//m_Enemies.emplace_back(Cultist, Vec3f{20, 2, -17});
	//m_Enemies.emplace_back(Cultist, Vec3f{22, 2, -17});
	//m_Enemies.emplace_back(Cultist, Vec3f{24, 2, -17});
	//m_Enemies.emplace_back(Cultist, Vec3f{26, 2, -17});

	//m_Enemies.emplace_back(Cultist, Vec3f{20, 2, -15});
	//m_Enemies.emplace_back(Cultist, Vec3f{22, 2, -15});
	//m_Enemies.emplace_back(Cultist, Vec3f{24, 2, -15});
	//m_Enemies.emplace_back(Cultist, Vec3f{26, 2, -15});

	m_Enemies.emplace_back(Cultist, Vec3f{-36, 9, 2});
	m_Enemies.emplace_back(Cultist, Vec3f{-38, 9, 2});
	m_Enemies.emplace_back(Cultist, Vec3f{-40, 9, 2});
	m_Enemies.emplace_back(Cultist, Vec3f{-42, 9, 2});

	m_Enemies.emplace_back(Cultist, Vec3f{-44, 9, 2});
	m_Enemies.emplace_back(Cultist, Vec3f{-46, 9, 2});
	m_Enemies.emplace_back(Cultist, Vec3f{-45, 9, 2});
	m_Enemies.emplace_back(Cultist, Vec3f{-48, 9, 2});


	for(auto P : App.GetPlayers())
	{
		P->Reset();
		//P->SetPosition({27, 2, -15});
		P->SetPosition({-49, 9, 2});
		//P->SetPosition({47, 23, -66});
		P->SetRotation({0, -3.14f});
		P->m_Debug = false;
	}


	m_Items.emplace_back(ItemType::Ammo, Vec3f{-49, 5.8f, 4});
	m_Items.emplace_back(ItemType::Health, Vec3f{-49, 5.8f, 0});
}

void Map::Draw(Shader &Shader, bool Wireframe) const
{
	glBindVertexArray(m_VAO);
	if(Wireframe)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	glDrawElements(GL_TRIANGLES, m_Meshes[0]->Indices.size(), GL_UNSIGNED_INT, (void*)0);

	if(Wireframe)
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	//m_Exit.Draw(Shader);
}

bool Map::Attack(Vec3f Origin, Vec3f Dir, float Damage, AttackType, float &t)
{
	bool WallHit = false;
	EnemyClass *EHit = nullptr;
	PlayerClass *PHit = nullptr;
	if(m_Meshes[0]->RayCollision(Origin, Dir, t))
	{
		WallHit = true;
	}
	for(int i = 0; i < m_Enemies.size(); ++i)
	{
		if(m_Enemies[i].RayCollision(Origin, Dir, t))
		{
			EHit = &m_Enemies[i];
		}
	}
	for(int i = 0; i < App.GetPlayers().size(); ++i)
	{
		if(App.GetPlayers()[i]->RayCollision(Origin, Dir, t))
		{
			PHit = App.GetPlayers()[i];
		}
	}
	DebugDrawer.DrawLine(Origin+Vec3f{0.0001f,0.0001f,0.0001f}, Origin+t*Dir, {1,0,0});
	bool hit = false;
	Vec3f Color;
	float Speed = 1;
	if(PHit != nullptr)			//Inverse order to checks, If hit has to be closer than the next category
	{
		if(App.GetGameType() == GameType::Host)
		{
			GameEvent E = CreateHealthEvent(PHit->GetId(), -Damage);
			App.AddEvent(E);
		}
		DebugDrawer.DrawPoint(Origin + t*Dir, {1,0,1});
		hit = true;
		Color = {0.8f,0,0};
	}
	else if(EHit != nullptr)
	{
		if(App.GetGameType() == GameType::Host)
		{
			EHit->Damage(Damage);
		}
		DebugDrawer.DrawPoint(Origin + t*Dir, {1,0,1});
		hit = true;
		Color = {0.8f,0,0};
	}
	else if(WallHit)
	{
		DebugDrawer.DrawPoint(Origin + t*Dir, {1,0,1});
		hit = true;
		Color = {0.6f,0.3f,0};
		Speed = 3;
	}
	if(hit)
	{
		Vec3f R1 = RandGen.GetV3f(-1, 1);
		Vec3f R2 = RandGen.GetV3f(-1, 1);
		Vec3f R3 = RandGen.GetV3f(-1, 1);
		Vec3f Time = RandGen.GetV3f(0.5f, 1);

		App.AddParticle(Origin + (t-0.001f)*Dir, Color, Vec3f{0,1,0}+R1*Speed-Dir/2, Time.x);
		App.AddParticle(Origin + (t-0.001f)*Dir, Color, Vec3f{0,1,0}+R2*Speed-Dir/2, Time.y);
		App.AddParticle(Origin + (t-0.001f)*Dir, Color, Vec3f{0,1,0}+R3*Speed-Dir/2, Time.z);
	}

	return hit;
}

Vec2V3 Map::HighLight(Vec3f Origin, Vec3f Dir, float &MaxRange)
{
	if(m_Exit.RayCollision(Origin, Dir, MaxRange))
		return m_Exit.GetAABB();
	else
		return Vec2V3{};
}

void Map::SetState(uint32_t Id, CharacterState State)
{
	for(auto &E : m_Enemies)
	{
		if(E.GetId() == Id)
		{
			E.SetState(State);
			break;
		}
	}
}

//Only updates collision box
void MapObject::SetMesh(Mesh *mesh)
{ 
	Vec3f Min = {FLT_MAX, FLT_MAX, FLT_MAX};
	Vec3f Max = {-FLT_MAX, -FLT_MAX, -FLT_MAX};
	for(auto &V : mesh->Vertices)
	{
		if(V.Position.x < Min.x)
			Min.x = V.Position.x;
		if(V.Position.y < Min.y)
			Min.y = V.Position.y;
		if(V.Position.z < Min.z)
			Min.z = V.Position.z;

		if(V.Position.x > Max.x)
			Max.x = V.Position.x;
		if(V.Position.y > Max.y)
			Max.y = V.Position.y;
		if(V.Position.z > Max.z)
			Max.z = V.Position.z;
	}
	m_AABB[0] = Min;
	m_AABB[1] = Max;
}

bool MapObject::RayCollision(Vec3f Origin, Vec3f Direction, float &Max)
{
	float Min = 0;
	return RayAABBCollisionTest(Origin, Direction, m_AABB, Min, Max);
}

bool Map::Interact(Vec3f Origin, Vec3f Dir, float &t)
{
	if(m_Exit.RayCollision(Origin, Dir, t))
	{
		m_Exit.Use();
		return true;
	}
	return false;
}

Vec2V3 MapObject::GetAABB()
{
	return Vec2V3{m_AABB[0], m_AABB[1]};
}
